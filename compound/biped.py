"""
import sys
path = 'e:/'
if path not in sys.path:
    sys.path.append(path)

import xrig.compound.biped
reload(xrig.compound.biped)

xrig.compound.biped.Biped('biped').create()
"""
import time

import maya.cmds as cmds

import xrig.base.xnode
import xrig.util.maya_api
import xrig.base.control
import xrig.base.joint
import xrig.base.limb
import xrig.base.xnode
import xrig.limb.spine
import xrig.limb.neck
import xrig.limb.leg
import xrig.limb.foot
import xrig.limb.fk_node
import xrig.limb.ik_fk_chain
import xrig.limb.leg_new

reload(xrig.base.xnode)
reload(xrig.util.maya_api)
reload(xrig.base.control)
reload(xrig.base.joint)
reload(xrig.base.limb)
reload(xrig.base.xnode)
reload(xrig.limb.spine)
reload(xrig.limb.neck)
reload(xrig.limb.leg)
reload(xrig.limb.foot)
reload(xrig.limb.fk_node)
reload(xrig.limb.ik_fk_chain)
reload(xrig.limb.leg_new)


# todo: add solve joints to spine
# todo: seperate ik/fk chains
# todo: consistent naming convention across all joints
# todo: make constraints and ikHandles an XNode type
# todo: rename foot nodes to foot

class Biped(object):
    def __init__(self, name, prefix='', mirror_axis='x'):
        self.name = name
        self.prefix = prefix
        self.mirror_axis = mirror_axis

    def create(self):
        start_time = time.time()
        print self.name
        print self.prefix

        cmds.file(new=True, force=True)
        cmds.file('E:/xrig/arm_start.ma', open=True, force=True)

        # --------------------------------------------------------------------------------------------------------------
        # create spine
        # --------------------------------------------------------------------------------------------------------------
        biped_spine = xrig.limb.spine.Spine(spine_start='spine_start', spine_end='spine_end')
        biped_spine.create()
        biped_spine.create_env_jnts()

        # --------------------------------------------------------------------------------------------------------------
        # create neck and head
        # --------------------------------------------------------------------------------------------------------------
        biped_neck = xrig.limb.neck.Neck('neck_start', 'neck_end')
        biped_neck.create()
        biped_neck.hook(biped_spine)

        # --------------------------------------------------------------------------------------------------------------
        # create leg and reverse foot
        # --------------------------------------------------------------------------------------------------------------
        left_leg = xrig.limb.leg_new.LegNew(prefix='l_',
                                            leg_hip='leg_start',
                                            leg_knee='leg_mid',
                                            leg_ankle='leg_end',
                                            leg_ball='foot_mid',
                                            leg_toe='foot_end',
                                            leg_in='foot_in',
                                            leg_out='foot_out',
                                            leg_heel='foot_heel')
        left_leg.create()

        # --------------------------------------------------------------------------------------------------------------
        # mirror leg
        # --------------------------------------------------------------------------------------------------------------
        left_leg.mirror()

        # --------------------------------------------------------------------------------------------------------------
        # create arm
        # --------------------------------------------------------------------------------------------------------------
        cmds.setAttr('arm_mid.translateZ', 0.0)
        left_arm = xrig.limb.ik_fk_chain.IkFkChain('arm', 'arm_start', 'arm_mid', 'arm_end', prefix='l_')
        left_arm.create()

        # --------------------------------------------------------------------------------------------------------------
        # mirror arm
        # --------------------------------------------------------------------------------------------------------------
        left_arm.mirror()

        # --------------------------------------------------------------------------------------------------------------
        # print rig build info
        # --------------------------------------------------------------------------------------------------------------
        cmds.parent('hip_geo', 'spine_ik_hip_ctrl_jnt')
        cmds.parent('shoulder_geo', 'spine_ik_chest_ctrl_jnt')
        cmds.parent('spine1_geo', 'spine_ik_00_jnt')
        cmds.parent('spine2_geo', 'spine_ik_01_jnt')
        cmds.parent('spine3_geo', 'spine_ik_02_jnt')
        cmds.parent('spine4_geo', 'spine_ik_03_jnt')
        cmds.parent('spine5_geo', 'spine_ik_04_jnt')
        cmds.parent('spine6_geo', 'spine_ik_05_jnt')

        print('\n' * 2)
        print('-' * 80)
        print('Rig Build complete! Time elapsed: {} seconds'.format(time.time() - start_time))
        print('-' * 80)
        print('\n' * 2)

        return True
