import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
import maya.OpenMayaAnim as OpenMayaAnim


def get_mobject(node):
    sel = OpenMaya.MSelectionList()
    OpenMaya.MGlobal.getSelectionListByName(node, sel)
    mobj = OpenMaya.MObject()
    sel.getDependNode(0, mobj)
    return mobj


def get_mdagpath(node):
    sel = OpenMaya.MSelectionList()
    OpenMaya.MGlobal.getSelectionListByName(node, sel)
    mdag = OpenMaya.MDagPath()
    sel.getDagPath(0, mdag)
    return mdag


def get_mplug(node, attr):
    mobj = get_mobject(node)
    depend_node_fn = OpenMaya.MFnDependencyNode(mobj)
    mplug = depend_node_fn.findPlug(attr)
    return mplug


def get_shapes(node):
    mdag = get_mdagpath(node)

    util = OpenMaya.MScriptUtil()
    util.createFromInt(0)
    util_ptr = util.asUintPtr()
    mdag.numberOfShapesDirectlyBelow(util_ptr)
    n = OpenMaya.MScriptUtil(util_ptr).asUint()

    shapes = []
    for i in range(n):
        mdag.extendToShapeDirectlyBelow(i)
        shapes.append(mdag.fullPathName())

    return shapes


def create_mvector(node):
    node_pos = cmds.xform(node, translation=True, worldSpace=True, query=True)
    mvector = OpenMaya.MVector(node_pos[0], node_pos[1], node_pos[2])
    return mvector
