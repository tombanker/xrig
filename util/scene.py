import maya.cmds as cmds


def get_node_from_attribute(attr):
    node_list = []
    for node in cmds.ls():
        if cmds.attributeQuery(attr, node=node, exists=True):
            node_list.append(node)
    return node_list


def get_controls():
    node_list = []
    for node in cmds.ls(type='transform'):
        if cmds.attributeQuery('metadata_type', node=node, exists=True):
            if cmds.getAttr('{}.{}'.format(node, 'metadata_type')) == 'control':
                node_list.append(node)

    return node_list
