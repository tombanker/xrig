"""
Module Docstring
"""

__author__ = 'tbanker'
__version__ = "3.2.5"

from xrig.base.xnode import XNode
import xrig.base.joint
from xrig.base.control import Control
import xrig.base.limb
import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
import xrig.util.maya_api
import xrig.limb.ik_fk_chain


class Foot(xrig.base.limb.Limb):
    def __init__(self, leg_limb, leg_ball, leg_toe, leg_in, leg_out, leg_heel, prefix=''):
        super(Foot, self).__init__(name='foot')
        self.prefix = prefix
        self.name = '{}{}'.format(self.prefix, self.name)

        self.leg_limb = leg_limb
        self.leg_ball = leg_ball
        self.leg_toe = leg_toe
        self.leg_in = leg_in
        self.leg_out = leg_out
        self.leg_heel = leg_heel

    def create(self):
        # --------------------------------------------------------------------------------------------------------------
        # create ik joints
        # --------------------------------------------------------------------------------------------------------------
        leg_ik_ball_jnt = xrig.base.joint.Joint('{}_ik_ball_jnt'.format(self.name),
                                                worldMatrix=self.leg_ball.worldMatrix,
                                                parent=self.leg_limb.limb_ik_tip_jnt)

        leg_ik_toe_jnt = xrig.base.joint.Joint('{}_ik_toe_jnt'.format(self.name),
                                               worldMatrix=self.leg_toe.worldMatrix, parent=leg_ik_ball_jnt)

        self.leg_limb.limb_ik_base_jnt.orient('xyz', 'xup', True, True)

        # --------------------------------------------------------------------------------------------------------------
        # setup foot ik handles
        # --------------------------------------------------------------------------------------------------------------
        __ik_handle = cmds.ikHandle(name='{}_ik_ball_handle'.format(self.name),
                                    startJoint=self.leg_limb.limb_ik_tip_jnt,
                                    endEffector=leg_ik_ball_jnt,
                                    solver='ikSCsolver')

        foot_ik_ball_handle = XNode(__ik_handle[0])
        foot_ik_ball_handle_eff = XNode(__ik_handle[1])
        foot_ik_ball_handle_eff.name = '{}_ik_ball_handle_eff'.format(self.name)

        __ik_handle = cmds.ikHandle(name='{}_ik_toe_handle'.format(self.name),
                                    startJoint=leg_ik_ball_jnt,
                                    endEffector=leg_ik_toe_jnt,
                                    solver='ikSCsolver')

        foot_ik_toe_handle = XNode(__ik_handle[0])
        foot_ik_toe_handle_eff = XNode(__ik_handle[1])
        foot_ik_toe_handle_eff.name = '{}_ik_mid_handle_eff'.format(self.name)

        # --------------------------------------------------------------------------------------------------------------
        # setup reverse foot nulls
        # --------------------------------------------------------------------------------------------------------------
        foot_ik_ankle_null = XNode('{}_ik_ankle_null'.format(self.name), worldMatrix=self.leg_limb.limb_tip.worldMatrix)

        foot_ik_heel_null = XNode('{}_ik_heel_null'.format(self.name), worldMatrix=self.leg_heel.worldMatrix)

        foot_ik_out_null = XNode('{}_ik_out_null'.format(self.name), worldMatrix=self.leg_out.worldMatrix,
                                 parent=foot_ik_heel_null)

        foot_ik_in_null = XNode('{}_ik_in_null'.format(self.name), worldMatrix=self.leg_in.worldMatrix,
                                parent=foot_ik_out_null)

        foot_ik_toe_null = XNode('{}_ik_toe_null'.format(self.name), worldMatrix=self.leg_toe.worldMatrix,
                                 parent=foot_ik_in_null, rotateOrder=2)

        foot_ik_ball_null = XNode('{}_ik_ball_null'.format(self.name), worldMatrix=self.leg_ball.worldMatrix,
                                  parent=foot_ik_toe_null)

        foot_ik_ankle_stretch_null = XNode('{}_ik_ankle_stretch_null'.format(self.name),
                                           worldMatrix=self.leg_limb.limb_tip.worldMatrix,
                                           parent=foot_ik_ball_null)

        foot_ik_toe_raise_null = XNode('{}_ik_toe_raise_null'.format(self.name),
                                       worldMatrix=self.leg_ball.worldMatrix,
                                       parent=foot_ik_toe_null)

        self.leg_limb.limb_ik_handle.parent = foot_ik_ball_null
        foot_ik_ball_handle.parent = foot_ik_ball_null
        foot_ik_toe_handle.parent = foot_ik_toe_raise_null
        foot_ik_heel_null.parent = foot_ik_ankle_null
        foot_ik_ankle_null.parent = self.leg_limb.limb_do_not_touch_grp

        # cmds.pointConstraint(leg_ik_ctrl, foot_ik_ankle_null)
        # cmds.orientConstraint(leg_ik_ctrl, foot_ik_ankle_null)
        cmds.pointConstraint(self.leg_limb.limb_ik_ctrl, foot_ik_ankle_null)
        cmds.orientConstraint(self.leg_limb.limb_ik_ctrl, foot_ik_ankle_null)

        # --------------------------------------------------------------------------------------------------------------
        # setup reverse foot roll control
        # --------------------------------------------------------------------------------------------------------------
        # todo: change these attr names
        # add attributes
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='foot_roll_controls', attributeType='enum',
                     enumName='----------')
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='roll', attributeType='float', keyable=True, min=-90, max=180)
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='ball_roll_angle', attributeType='float', keyable=True,
                     defaultValue=45)
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='toe_roll_angle', attributeType='float', keyable=True,
                     defaultValue=70)
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='side_tilt', attributeType='float', keyable=True, min=-360,
                     max=360)
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='ball_lean', attributeType='float', keyable=True)
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='ball_spin', attributeType='float', keyable=True)
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='toe_lean', attributeType='float', keyable=True)
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='toe_spin', attributeType='float', keyable=True)
        cmds.addAttr(self.leg_limb.limb_ik_ctrl, longName='toe_raise', attributeType='float', keyable=True)

        cmds.setAttr('{}.foot_roll_controls'.format(self.leg_limb.limb_ik_ctrl), channelBox=True)
        # --------------------------------------------------------------------------------------------------------------
        # setup reverse foot roll nodes
        # --------------------------------------------------------------------------------------------------------------
        # heel clamp, heel result
        foot_roll_heel_clamp = XNode('{}_roll_heel_clamp'.format(self.name), node_type='clamp', minR=-90.0)
        self.leg_limb.limb_ik_ctrl.roll >> foot_roll_heel_clamp.inputR
        foot_roll_heel_clamp.outputR >> foot_ik_heel_null.rotateX

        # ball clamp
        foot_roll_ball_clamp = XNode('{}_roll_ball_clamp'.format(self.name), node_type='clamp')
        self.leg_limb.limb_ik_ctrl.roll >> foot_roll_ball_clamp.inputR
        self.leg_limb.limb_ik_ctrl.ball_roll_angle >> foot_roll_ball_clamp.maxR

        # ball normalize
        foot_roll_ball_normalize = XNode('{}_roll_ball_normalize'.format(self.name), node_type='setRange', maxX=1.0)
        foot_roll_ball_clamp.minR >> foot_roll_ball_normalize.oldMinX
        foot_roll_ball_clamp.maxR >> foot_roll_ball_normalize.oldMaxX
        foot_roll_ball_clamp.inputR >> foot_roll_ball_normalize.valueX

        # toe clamp
        foot_roll_toe_clamp = XNode('{}_roll_toe_clamp'.format(self.name), node_type='clamp')
        self.leg_limb.limb_ik_ctrl.roll >> foot_roll_toe_clamp.inputR
        self.leg_limb.limb_ik_ctrl.ball_roll_angle >> foot_roll_toe_clamp.minR
        self.leg_limb.limb_ik_ctrl.toe_roll_angle >> foot_roll_toe_clamp.maxR

        # heel normalize
        foot_roll_heel_normalize = XNode('{}_roll_heel_normalize'.format(self.name), node_type='setRange', maxX=1.0)
        foot_roll_toe_clamp.minR >> foot_roll_heel_normalize.oldMinX
        foot_roll_toe_clamp.maxR >> foot_roll_heel_normalize.oldMaxX
        foot_roll_toe_clamp.inputR >> foot_roll_heel_normalize.valueX

        # toe result
        foot_roll_toe_result_mult = XNode('{}_roll_toe_result_mult'.format(self.name), node_type='multiplyDivide')
        foot_roll_heel_normalize.outValueX >> foot_roll_toe_result_mult.input1X
        foot_roll_heel_clamp.inputR >> foot_roll_toe_result_mult.input2X
        foot_roll_toe_result_mult.outputX >> foot_ik_toe_null.rotateX

        # ball invert
        foot_roll_ball_invert = XNode('{}_roll_ball_invert'.format(self.name), node_type='plusMinusAverage',
                                      operation=2)
        cmds.setAttr('{}.input1D[0]'.format(foot_roll_ball_invert), 1.0)
        cmds.setAttr('{}.input1D[1]'.format(foot_roll_ball_invert), 1.0)
        # foot_roll_heel_normalize.outValueX >> foot_roll_ball_invert.input1D
        foot_roll_heel_normalize.connect('outValueX', foot_roll_ball_invert, 'input1D[1]')

        # ball invert mult
        foot_roll_ball_invert_mult = XNode('{}_roll_ball_invert_mult'.format(self.name), node_type='multiplyDivide')
        foot_roll_ball_normalize.outValueX >> foot_roll_ball_invert_mult.input1X
        foot_roll_ball_invert.output1D >> foot_roll_ball_invert_mult.input2X

        # ball result
        foot_roll_ball_result_mult = XNode('{}_roll_ball_result_mult'.format(self.name), node_type='multiplyDivide')
        foot_roll_ball_invert_mult.outputX >> foot_roll_ball_result_mult.input1X
        foot_roll_ball_result_mult.outputX >> foot_ik_ball_null.rotateX
        self.leg_limb.limb_ik_ctrl.roll >> foot_roll_ball_result_mult.input2X

        # --------------------------------------------------------------------------------------------------------------
        # connect reverse foot roll attributes
        # --------------------------------------------------------------------------------------------------------------
        self.leg_limb.limb_ik_ctrl.ball_spin >> foot_ik_ball_null.rotateY
        self.leg_limb.limb_ik_ctrl.ball_lean >> foot_ik_ball_null.rotateZ

        self.leg_limb.limb_ik_ctrl.toe_spin >> foot_ik_toe_null.rotateY
        self.leg_limb.limb_ik_ctrl.toe_lean >> foot_ik_toe_null.rotateZ

        self.leg_limb.limb_ik_ctrl.toe_raise >> foot_ik_toe_raise_null.rotateX

        # --------------------------------------------------------------------------------------------------------------
        # setup reverse foot roll side tilt
        # --------------------------------------------------------------------------------------------------------------
        foot_roll_tilt_in_remap = XNode('{}_roll_tilt_in_remap'.format(self.name), node_type='remapValue',
                                        inputMax=360, outputMax=360)
        self.leg_limb.limb_ik_ctrl.side_tilt >> foot_roll_tilt_in_remap.inputValue
        foot_roll_tilt_in_remap.outColorR >> foot_ik_in_null.rotateZ

        foot_roll_tilt_out_remap = XNode('{}_roll_tilt_out_remap'.format(self.name), node_type='remapValue',
                                         inputMin=-360, outputMin=-360)
        self.leg_limb.limb_ik_ctrl.side_tilt >> foot_roll_tilt_out_remap.inputValue
        foot_roll_tilt_out_remap.outColorR >> foot_ik_out_null.rotateZ

        # --------------------------------------------------------------------------------------------------------------
        # override previous stretch calculation nodes with new foot roll ankle node
        # --------------------------------------------------------------------------------------------------------------
        foot_ik_ankle_stretch_null.worldMatrix >> self.leg_limb.limb_ik_ctrl_stretch_ctrl_dist.inMatrix2
        foot_ik_ankle_stretch_null.worldMatrix >> self.leg_limb.limb_ik_pv_ctrl_pin_tip_dist.inMatrix2

    def hook(self):
        pass
