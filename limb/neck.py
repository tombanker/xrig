import maya.cmds as cmds
import xrig.base.xnode
import xrig.base.joint
import xrig.base.limb
import xrig.base.control


class Neck(xrig.base.limb.Limb):
    def __init__(self, neck_start, neck_end):
        super(Neck, self).__init__(name='neck')
        self.neck_start = xrig.base.xnode.XNode(name=neck_start)
        self.neck_end = xrig.base.xnode.XNode(name=neck_end)

        self.neck_ctrl_grp = None
        self.neck_end_jnt = None
        self.head_ctrl = None
        self.neck_base_fk_jnt = None
        self.neck_ctrl = None
        self.head_neck_space_grp = None

    def create(self):
        # --------------------------------------------------------------------------------------------------------------
        # create groups
        # --------------------------------------------------------------------------------------------------------------
        neck_grp = xrig.base.xnode.XNode(name='{}_grp'.format(self.name))
        self.neck_ctrl_grp = xrig.base.xnode.XNode(name='{}_ctrl_grp'.format(self.name), parent=neck_grp)
        neck_do_not_touch_grp = xrig.base.xnode.XNode(name='{}_do_not_touch_grp'.format(self.name), parent=neck_grp)

        # --------------------------------------------------------------------------------------------------------------
        # create ik joints
        # --------------------------------------------------------------------------------------------------------------
        neck_start_jnt = xrig.base.joint.Joint(name='{}_ik_00_jnt'.format(self.name),
                                               node_type='joint',
                                               worldMatrix=self.neck_start.worldMatrix)

        self.neck_end_jnt = xrig.base.joint.Joint(name='{}_ik_04_jnt'.format(self.name),
                                                  node_type='joint',
                                                  worldMatrix=self.neck_end.worldMatrix)

        # --------------------------------------------------------------------------------------------------------------
        # setup ik spline
        # --------------------------------------------------------------------------------------------------------------
        xrig.base.joint.insert_joints(neck_start_jnt, self.neck_end_jnt, num_mid_joints=2,
                                      prefix='{}_ik'.format(self.name))
        neck_start_jnt.orient('xyz', 'zup', True, True)

        __ik_handle = cmds.ikHandle(name='{}_ik_handle'.format(self.name),
                                    startJoint=neck_start_jnt,
                                    endEffector=self.neck_end_jnt,
                                    solver='ikSplineSolver')

        neck_ik_handle = xrig.base.xnode.XNode(__ik_handle[0])
        neck_ik_handle_eff = xrig.base.xnode.XNode(__ik_handle[1])
        neck_ik_handle_crv = xrig.base.xnode.XNode(__ik_handle[2])
        neck_ik_handle_eff.name = '{}_ik_handle_eff'.format(self.name)
        neck_ik_handle_crv.name = '{}_ik_handle_crv'.format(self.name)

        # create two joints and skin them to the spline curve
        neck_ik_base_crv_jnt = xrig.base.joint.Joint('{}_ik_base_ctrl_jnt'.format(self.name),
                                                     rotateOrder=2,
                                                     jointOrient=neck_start_jnt.jointOrient)
        neck_ik_base_crv_jnt.worldMatrix = neck_start_jnt.worldMatrix

        neck_ik_tip_crv_jnt = xrig.base.joint.Joint('{}_ik_tip_ctrl_jnt'.format(self.name),
                                                    rotateOrder=2,
                                                    jointOrient=self.neck_end_jnt.jointOrient)
        neck_ik_tip_crv_jnt.worldMatrix = self.neck_end_jnt.worldMatrix

        cmds.skinCluster(neck_ik_base_crv_jnt.name, neck_ik_tip_crv_jnt.name, neck_ik_handle_crv.name)

        # --------------------------------------------------------------------------------------------------------------
        # create ik neck controls
        # --------------------------------------------------------------------------------------------------------------
        self.head_ctrl = xrig.base.control.Control(name='head_ctrl',
                                                   control_shape='cube',
                                                   rotateOrder='zxy')
        self.head_ctrl.create_null_grps()
        self.head_ctrl.null_grp.worldMatrix = self.neck_end_jnt.worldMatrix

        # parent constrain controls to control joints
        cmds.parentConstraint(self.head_ctrl.name, neck_ik_tip_crv_jnt, maintainOffset=True)

        # add ik spline handle advanced twist
        neck_ik_handle.dTwistControlEnable = 1
        neck_ik_handle.dWorldUpType = 4
        neck_ik_handle.dForwardAxis = 0
        neck_ik_handle.dWorldUpAxis = 3
        neck_ik_handle.dWorldUpVector = (0, 0, 1)
        neck_ik_handle.dWorldUpVectorEnd = (0, 0, 1)
        neck_ik_base_crv_jnt.connect('worldMatrix[0]', neck_ik_handle, 'dWorldUpMatrix')
        neck_ik_tip_crv_jnt.connect('worldMatrix[0]', neck_ik_handle, 'dWorldUpMatrixEnd')

        # --------------------------------------------------------------------------------------------------------------
        # setup fk spine chain
        # --------------------------------------------------------------------------------------------------------------
        self.neck_base_fk_jnt = xrig.base.joint.Joint(name='{}_base_fk_jnt'.format(self.name),
                                                      jointOrient=neck_start_jnt.jointOrient,
                                                      worldMatrix=neck_start_jnt.worldMatrix,
                                                      rotateOrder='yzx')

        neck_tip_fk_jnt = xrig.base.joint.Joint(name='{}_tip_fk_jnt'.format(self.name),
                                                jointOrient=self.neck_end_jnt.jointOrient,
                                                worldMatrix=self.neck_end_jnt.worldMatrix,
                                                rotateOrder='yzx',
                                                parent=self.neck_base_fk_jnt)

        self.neck_base_fk_jnt.freeze()
        self.neck_base_fk_jnt.orient('xyz', 'zup', True, True)

        # --------------------------------------------------------------------------------------------------------------
        # create fk spine controls
        # --------------------------------------------------------------------------------------------------------------
        self.neck_ctrl = xrig.base.control.Control(name='{}_ctrl'.format(self.name),
                                                   worldMatrix=neck_start_jnt.worldMatrix,
                                                   rotateOrder='zxy')
        self.neck_ctrl.create_null_grps()

        cmds.parentConstraint(self.neck_ctrl, neck_ik_base_crv_jnt, maintainOffset=True)
        cmds.parentConstraint(self.neck_ctrl, self.neck_base_fk_jnt, maintainOffset=True)

        # --------------------------------------------------------------------------------------------------------------
        # add head control space switching
        # --------------------------------------------------------------------------------------------------------------
        self.head_neck_space_grp = xrig.base.xnode.XNode('head_{}_space_grp'.format(self.name),
                                                         parent=self.neck_base_fk_jnt,
                                                         worldMatrix=self.neck_end_jnt.worldMatrix)

        cmds.addAttr(self.head_ctrl, longName='translation_space', shortName='translation_space', attributeType='enum',
                     enumName='neck', keyable=True)

        cmds.addAttr(self.head_ctrl, longName='rotation_space', shortName='rotation_space', attributeType='enum',
                     enumName='neck', keyable=True)

        self.head_ctrl.add_space_switching(dst_node=self.head_ctrl.const_grp,
                                           dst_attr='{}.translation_space'.format(self.head_ctrl),
                                           constraint_type='point',
                                           targets=[self.head_neck_space_grp])

        self.head_ctrl.add_space_switching(dst_node=self.head_ctrl.const_grp,
                                           dst_attr='{}.rotation_space'.format(self.head_ctrl),
                                           constraint_type='orient',
                                           targets=[self.head_neck_space_grp])

        # --------------------------------------------------------------------------------------------------------------
        # create stretchy neck utility nodes
        # --------------------------------------------------------------------------------------------------------------
        neck_crv_info = xrig.base.xnode.XNode('{}_crv_info'.format(self.name), node_type='curveInfo')
        neck_ik_handle_crv.connect('worldSpace[0]', neck_crv_info, 'inputCurve')

        neck_stretch_factor_div = xrig.base.xnode.XNode('{}_stretch_factor_div'.format(self.name),
                                                        node_type='multiplyDivide',
                                                        operation=2)

        neck_crv_info.connect('arcLength', neck_stretch_factor_div, 'input1X')
        neck_stretch_factor_div.input2X = neck_stretch_factor_div.input1X

        for jnt in neck_start_jnt.chain:
            neck_stretch_factor_div.connect('outputX', jnt, 'scaleX')

        # --------------------------------------------------------------------------------------------------------------
        # fix global scale offset in spline curve info arc length
        # --------------------------------------------------------------------------------------------------------------
        neck_global_scale_normalize_div = xrig.base.xnode.XNode('{}_global_scale_normalize_div'.format(self.name),
                                                                node_type='multiplyDivide',
                                                                operation=2)
        neck_crv_info.connect('arcLength', neck_global_scale_normalize_div, 'input1X')
        neck_grp.connect('scaleY', neck_global_scale_normalize_div, 'input2X')
        neck_global_scale_normalize_div.connect('outputX', neck_stretch_factor_div, 'input1X')
        neck_ik_handle_crv.inheritsTransform = 0

        # --------------------------------------------------------------------------------------------------------------
        # group nodes
        # --------------------------------------------------------------------------------------------------------------
        neck_start_jnt.parent = neck_do_not_touch_grp
        neck_ik_base_crv_jnt.parent = neck_do_not_touch_grp
        neck_ik_tip_crv_jnt.parent = neck_do_not_touch_grp
        neck_ik_handle.parent = neck_do_not_touch_grp
        self.neck_base_fk_jnt.parent = neck_do_not_touch_grp
        neck_ik_handle_crv.parent = neck_do_not_touch_grp

        self.neck_ctrl.null_grp.parent = self.neck_ctrl_grp
        self.head_ctrl.null_grp.parent = self.neck_ctrl_grp

    def hook(self, spine_limb):
        # print '~~~', spine_limb, vars(spine_limb)
        cmds.parentConstraint(spine_limb.spine_ik_chest_crv_jnt.name, self.neck_ctrl.anim_grp, maintainOffset=True)

        # --------------------------------------------------------------------------------------------------------------
        # add head control space switching nodes
        # --------------------------------------------------------------------------------------------------------------
        head_chest_space_grp = xrig.base.xnode.XNode('head_chest_space_grp',
                                                     worldMatrix=self.neck_end_jnt.worldMatrix,
                                                     parent=spine_limb.spine_ik_chest_crv_jnt)

        head_spine_space_grp = xrig.base.xnode.XNode('head_spine_space_grp',
                                                     worldMatrix=self.neck_end_jnt.worldMatrix,
                                                     parent=spine_limb.spine_do_not_touch_grp)

        cmds.parentConstraint(spine_limb.spine_body_ctrl, head_spine_space_grp, maintainOffset=True)

        # --------------------------------------------------------------------------------------------------------------
        # edit head control space switching attributes
        # --------------------------------------------------------------------------------------------------------------
        cmds.addAttr('{}.translation_space'.format(self.head_ctrl), attributeType='enum', enumName='neck:chest:spine',
                     keyable=True, edit=True)

        cmds.addAttr('{}.rotation_space'.format(self.head_ctrl), attributeType='enum', enumName='neck:chest:spine',
                     keyable=True, edit=True)

        # --------------------------------------------------------------------------------------------------------------
        # connect head control translation and rotation space switching
        # --------------------------------------------------------------------------------------------------------------
        self.head_ctrl.add_space_switching(self.head_ctrl.const_grp,
                                           dst_attr='{}.translation_space'.format(self.head_ctrl),
                                           constraint_type='point',
                                           targets=[self.head_neck_space_grp,
                                                    head_chest_space_grp,
                                                    head_spine_space_grp])

        self.head_ctrl.add_space_switching(dst_node=self.head_ctrl.const_grp,
                                           dst_attr='{}.rotation_space'.format(self.head_ctrl),
                                           constraint_type='orient',
                                           targets=[self.head_neck_space_grp,
                                                    head_chest_space_grp,
                                                    head_spine_space_grp])
