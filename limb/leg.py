"""
Module Docstring
"""

__author__ = 'tbanker'
__version__ = "1.0.0"

from xrig.base.xnode import XNode
import xrig.base.joint
from xrig.base.control import Control
import xrig.base.limb
import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
import xrig.util.maya_api


# todo: fk stretch
# todo: ik stretch - done
# todo: ik pole vector - done
# todo: dual knee
# todo: snappable knee - done
# todo: non-uniform no flip ik leg stretch and
# todo: leg global transform, space switching and
# todo: reverse foot rig - done
# todo: make this a combination of fk chain, ik chain w/ additional leg controls, switching added here

# todo: make a fk chain class (any number of inbetween joints, controls, etc...)
# todo: make a ik chain class (a root, tip, any number of inbetween joints, controls, etc...)
# todo: make a fk/ik chain class (combine two above limbs, add a slave joint chain controled by each)
# todo: call the fk/ik chain class, add in this class additional stuff for adding custom leg stuff and foot stuff

# todo: create a mult/div stretch factor calc node based on result joint, plug that into ik_ctrl stretch_factor attr

class Leg(xrig.base.limb.Limb):
    def __init__(self, leg_hip, leg_knee, leg_ankle, leg_ball, leg_toe, leg_in, leg_out, leg_heel, prefix='l_'):
        super(Leg, self).__init__(name='leg')
        self.prefix = prefix
        self.name = '{}{}'.format(self.prefix, self.name)

        self.leg_hip = XNode(name=leg_hip)
        self.leg_knee = XNode(name=leg_knee)
        self.leg_ankle = XNode(name=leg_ankle)

        self.leg_ball = XNode(name=leg_ball)
        self.leg_toe = XNode(name=leg_toe)
        self.leg_in = XNode(name=leg_in)
        self.leg_out = XNode(name=leg_out)
        self.leg_heel = XNode(name=leg_heel)

    def create(self):
        # --------------------------------------------------------------------------------------------------------------
        # create groups
        # --------------------------------------------------------------------------------------------------------------
        leg_grp = XNode('{}_grp'.format(self.name))
        leg_ctrl_grp = XNode('{}_ctrl_grp'.format(self.name), parent=leg_grp)
        leg_do_not_touch_grp = XNode('{}_do_not_touch_grp'.format(self.name), parent=leg_grp)

        # --------------------------------------------------------------------------------------------------------------
        # create solve joints
        # --------------------------------------------------------------------------------------------------------------
        leg_result_hip_jnt = xrig.base.joint.Joint('{}_result_hip_jnt'.format(self.name),
                                                   worldMatrix=self.leg_hip.worldMatrix)
        leg_result_knee_jnt = xrig.base.joint.Joint('{}_result_knee_jnt'.format(self.name),
                                                    worldMatrix=self.leg_knee.worldMatrix, parent=leg_result_hip_jnt)
        leg_result_ankle_jnt = xrig.base.joint.Joint('{}_result_ankle_jnt'.format(self.name),
                                                     worldMatrix=self.leg_ankle.worldMatrix, parent=leg_result_knee_jnt)
        leg_result_hip_jnt.orient('xyz', 'xup', True, True)

        # --------------------------------------------------------------------------------------------------------------
        # create ik joints
        # --------------------------------------------------------------------------------------------------------------
        leg_ik_hip_jnt = xrig.base.joint.Joint('{}_ik_hip_jnt'.format(self.name),
                                               worldMatrix=self.leg_hip.worldMatrix)
        leg_ik_knee_jnt = xrig.base.joint.Joint('{}_ik_knee_jnt'.format(self.name),
                                                worldMatrix=self.leg_knee.worldMatrix, parent=leg_ik_hip_jnt)
        leg_ik_ankle_jnt = xrig.base.joint.Joint('{}_ik_ankle_jnt'.format(self.name),
                                                 worldMatrix=self.leg_ankle.worldMatrix, parent=leg_ik_knee_jnt)

        leg_ik_ball_jnt = xrig.base.joint.Joint('{}_ik_ball_jnt'.format(self.name),
                                                worldMatrix=self.leg_ball.worldMatrix, parent=leg_ik_ankle_jnt)

        leg_ik_toe_jnt = xrig.base.joint.Joint('{}_ik_toe_jnt'.format(self.name),
                                               worldMatrix=self.leg_toe.worldMatrix, parent=leg_ik_ball_jnt)

        leg_ik_hip_jnt.orient('xyz', 'xup', True, True)

        # --------------------------------------------------------------------------------------------------------------
        # setup ik handles and control
        # --------------------------------------------------------------------------------------------------------------
        __ik_handle = cmds.ikHandle(name='{}_ik_handle'.format(self.name),
                                    startJoint=leg_ik_hip_jnt,
                                    endEffector=leg_ik_ankle_jnt,
                                    solver='ikRPsolver')

        leg_ik_handle = XNode(__ik_handle[0])
        leg_ik_handle_eff = XNode(__ik_handle[1])
        leg_ik_handle_eff.name = '{}_ik_handle_eff'.format(self.name)

        leg_ik_ctrl = Control('{}_ik_ctrl'.format(self.name),
                              control_shape='cube',
                              worldMatrix=leg_ik_ankle_jnt.worldMatrix,
                              rotateOrder='zxy')

        leg_ik_ctrl.rotate = (0.0, 0.0, 0.0)
        leg_ik_ctrl.create_null_grps()
        # cmds.parentConstraint(leg_ik_ctrl, leg_ik_handle)

        # add attributes
        cmds.addAttr(leg_ik_ctrl, longName='ik_chain_controls', attributeType='enum', enumName='----------',
                     keyable=False)
        cmds.addAttr(leg_ik_ctrl, longName='stretch', attributeType='float', keyable=True, min=0, max=1, defaultValue=1)
        cmds.addAttr(leg_ik_ctrl, longName='knee_pin', attributeType='float', keyable=True, min=0, max=1,
                     defaultValue=0)
        cmds.addAttr(leg_ik_ctrl, longName='knee_twist', attributeType='float', keyable=True)
        cmds.addAttr(leg_ik_ctrl, longName='stretch_amount', attributeType='float', keyable=False)

        cmds.setAttr('{}.ik_chain_controls'.format(leg_ik_ctrl), channelBox=True)
        cmds.setAttr('{}.stretch_amount'.format(leg_ik_ctrl), channelBox=True)

        # --------------------------------------------------------------------------------------------------------------
        # setup foot ik handles
        # --------------------------------------------------------------------------------------------------------------
        __ik_handle = cmds.ikHandle(name='{}_ik_ball_handle'.format(self.name),
                                    startJoint=leg_ik_ankle_jnt,
                                    endEffector=leg_ik_ball_jnt,
                                    solver='ikSCsolver')

        foot_ik_ball_handle = XNode(__ik_handle[0])
        foot_ik_ball_handle_eff = XNode(__ik_handle[1])
        foot_ik_ball_handle_eff.name = '{}_ik_ball_handle_eff'.format(self.name)

        __ik_handle = cmds.ikHandle(name='{}_ik_toe_handle'.format(self.name),
                                    startJoint=leg_ik_ball_jnt,
                                    endEffector=leg_ik_toe_jnt,
                                    solver='ikSCsolver')

        foot_ik_toe_handle = XNode(__ik_handle[0])
        foot_ik_toe_handle_eff = XNode(__ik_handle[1])
        foot_ik_toe_handle_eff.name = '{}_ik_mid_handle_eff'.format(self.name)

        # --------------------------------------------------------------------------------------------------------------
        # setup ik stretching
        # --------------------------------------------------------------------------------------------------------------
        # create a null object for the ik root joint
        leg_ik_root_null = XNode('{}_ik_root_null'.format(self.name),
                                 worldMatrix=leg_ik_hip_jnt.worldMatrix)

        # get distance between ik root and mid jnt
        leg_ik_ctrl_stretch_mid_dist = XNode('{}_stretch_mid_dist'.format(leg_ik_ctrl),
                                             node_type='distanceBetween')
        leg_ik_root_null.worldMatrix >> leg_ik_ctrl_stretch_mid_dist.inMatrix1
        leg_ik_knee_jnt.worldMatrix >> leg_ik_ctrl_stretch_mid_dist.inMatrix2

        # get distance between ik mid and end jnt
        leg_ik_ctrl_stretch_tip_dist = XNode('{}_stretch_tip_dist'.format(leg_ik_ctrl),
                                             node_type='distanceBetween')
        leg_ik_knee_jnt.worldMatrix >> leg_ik_ctrl_stretch_tip_dist.inMatrix1
        leg_ik_ankle_jnt.worldMatrix >> leg_ik_ctrl_stretch_tip_dist.inMatrix2

        # get distance between ik root and ik handle
        leg_ik_ctrl_stretch_ctrl_dist = XNode('{}_stretch_ctrl_dist'.format(leg_ik_ctrl),
                                              node_type='distanceBetween')
        leg_ik_root_null.worldMatrix >> leg_ik_ctrl_stretch_ctrl_dist.inMatrix1
        leg_ik_ctrl.worldMatrix >> leg_ik_ctrl_stretch_ctrl_dist.inMatrix2
        # leg_ik_handle.worldMatrix >> leg_ik_ctrl_stretch_ctrl_dist.inMatrix2

        # input the distance between ik root and ik handle; and total chain distance into a multiplyDivide node
        leg_ik_ctrl_stretch_factor_div = XNode('{}_stretch_factor_div'.format(leg_ik_ctrl),
                                               node_type='multiplyDivide',
                                               operation=2)
        leg_ik_ctrl_stretch_ctrl_dist.distance >> leg_ik_ctrl_stretch_factor_div.input1X
        leg_ik_ctrl_stretch_sum_dist = float(leg_ik_ctrl_stretch_mid_dist.distance.value +
                                             leg_ik_ctrl_stretch_tip_dist.distance.value)
        leg_ik_ctrl_stretch_factor_div.input2X = leg_ik_ctrl_stretch_sum_dist

        # create a condition node to not stretch if the distance between joints are shorter than the rest length
        leg_ik_ctrl_stretch_cond = XNode('{}_stretch_cond'.format(leg_ik_ctrl),
                                         node_type='condition',
                                         operation=2,
                                         secondTerm=leg_ik_ctrl_stretch_sum_dist)
        leg_ik_ctrl_stretch_ctrl_dist.distance >> leg_ik_ctrl_stretch_cond.firstTerm
        leg_ik_ctrl_stretch_factor_div.outputX >> leg_ik_ctrl_stretch_cond.colorIfTrueR
        leg_ik_ctrl_stretch_factor_div.outputX >> leg_ik_ctrl.stretch_amount

        # create a stretch factor multiply divide for the mid ik joint
        leg_ik_ctrl_stretch_mid_mult = XNode('{}_stretch_mid_mult'.format(leg_ik_ctrl),
                                             node_type='multiplyDivide',
                                             input2X=leg_ik_ctrl_stretch_mid_dist.distance)
        leg_ik_ctrl_stretch_cond.outColorR >> leg_ik_ctrl_stretch_mid_mult.input1X
        # create a stretch factor multiply divide for the end ik joint
        leg_ik_ctrl_stretch_tip_mult = XNode('{}_stretch_tip_mult'.format(leg_ik_ctrl),
                                             node_type='multiplyDivide',
                                             input2X=leg_ik_ctrl_stretch_tip_dist.distance)
        leg_ik_ctrl_stretch_cond.outColorR >> leg_ik_ctrl_stretch_tip_mult.input1X

        # create a blend colors node to blend between stretching or not stretching
        leg_ik_ctrl_stretch_blend = XNode('{}_stretch_blend'.format(leg_ik_ctrl),
                                          node_type='blendColors',
                                          color2R=leg_ik_ctrl_stretch_mid_dist.distance,
                                          color2G=leg_ik_ctrl_stretch_tip_dist.distance)
        leg_ik_ctrl.stretch >> leg_ik_ctrl_stretch_blend.blender
        leg_ik_ctrl_stretch_mid_mult.outputX >> leg_ik_ctrl_stretch_blend.color1R
        leg_ik_ctrl_stretch_tip_mult.outputX >> leg_ik_ctrl_stretch_blend.color1G

        # --------------------------------------------------------------------------------------------------------------
        # setup knee pole vector
        # --------------------------------------------------------------------------------------------------------------
        leg_ik_pv_ctrl = Control('{}_ik_pv_ctrl'.format(self.name),
                                 control_shape='half_diamond',
                                 worldMatrix=leg_ik_knee_jnt.worldMatrix)
        leg_ik_pv_ctrl.rotate = (0.0, 0.0, 0.0)

        leg_ik_pv_ctrl.translate = self.calculate_pv_ctrl_pos(leg_ik_hip_jnt, leg_ik_knee_jnt, leg_ik_ankle_jnt, 10.0)
        cmds.poleVectorConstraint(leg_ik_pv_ctrl, leg_ik_handle)
        leg_ik_pv_ctrl.create_null_grps()

        """
        # --------------------------------------------------------------------------------------------------------------
        # setup no flip knee
        # --------------------------------------------------------------------------------------------------------------
        leg_ik_pv_no_flip_ctrl = Control('{}_ik_pv_no_flip_ctrl'.format(self.name),
                                         worldMatrix=leg_ik_ankle_jnt.worldMatrix)
        leg_ik_pv_no_flip_ctrl.rotate = (0.0, 0.0, 0.0)
        leg_ik_pv_no_flip_ctrl.translateX += 100
        leg_ik_pv_no_flip_ctrl.create_null_grps()
        leg_ik_pv_no_flip_ctrl.anim_grp.translate = leg_ik_pv_no_flip_ctrl.null_grp.translate
        leg_ik_pv_no_flip_ctrl.null_grp.translate = (0.0, 0.0, 0.0)

        cmds.poleVectorConstraint(leg_ik_pv_no_flip_ctrl, leg_ik_handle)
        leg_ik_handle.twist = 90
        leg_ik_ctrl.knee_twist >> leg_ik_pv_no_flip_ctrl.ll_grp'rotateY')
        """

        # --------------------------------------------------------------------------------------------------------------
        # setup dual no flip knee
        # --------------------------------------------------------------------------------------------------------------
        # duplicate ik chain twice (function?)
        # duplicate special

        # --------------------------------------------------------------------------------------------------------------
        # setup knee pinning
        # --------------------------------------------------------------------------------------------------------------
        leg_ik_pv_ctrl_pin_root_dist = XNode('{}_pin_root_dist'.format(leg_ik_pv_ctrl),
                                             node_type='distanceBetween')
        leg_ik_root_null.worldMatrix >> leg_ik_pv_ctrl_pin_root_dist.inMatrix1
        leg_ik_pv_ctrl.worldMatrix >> leg_ik_pv_ctrl_pin_root_dist.inMatrix2

        leg_ik_pv_ctrl_pin_tip_dist = XNode('{}_pin_tip_dist'.format(leg_ik_pv_ctrl),
                                            node_type='distanceBetween')
        leg_ik_pv_ctrl.worldMatrix >> leg_ik_pv_ctrl_pin_tip_dist.inMatrix1
        # todo: note and test this change - reorder foot ik handle creation to higher up
        # leg_ik_handle_null = XNode('{}_null'.format(leg_ik_handle), worldMatrix=leg_ik_handle.worldMatrix,
        #                            parent=leg_ik_handle)
        leg_ik_ctrl.worldMatrix >> leg_ik_pv_ctrl_pin_tip_dist.inMatrix2
        # leg_ik_handle.worldMatrix >> leg_ik_pv_ctrl_pin_tip_dist.inMatrix2
        # leg_ik_handle_null.worldMatrix >> leg_ik_pv_ctrl_pin_tip_dist.inMatrix2
        # leg_ik_handle_null.worldMatrix >> leg_ik_pv_ctrl_pin_tip_dist.inMatrix2

        # create a blend colors node to blend between knee pinning
        leg_ik_pv_ctrl_pin_blend = XNode('{}_pin_blend'.format(leg_ik_pv_ctrl),
                                         node_type='blendColors',
                                         color2R=leg_ik_pv_ctrl_pin_root_dist.distance,
                                         color2G=leg_ik_pv_ctrl_pin_tip_dist.distance)

        leg_ik_ctrl.knee_pin >> leg_ik_pv_ctrl_pin_blend.blender
        leg_ik_pv_ctrl_pin_root_dist.distance >> leg_ik_pv_ctrl_pin_blend.color1R
        leg_ik_pv_ctrl_pin_tip_dist.distance >> leg_ik_pv_ctrl_pin_blend.color1G
        leg_ik_ctrl_stretch_blend.outputR >> leg_ik_pv_ctrl_pin_blend.color2R
        leg_ik_ctrl_stretch_blend.outputG >> leg_ik_pv_ctrl_pin_blend.color2G
        leg_ik_pv_ctrl_pin_blend.outputR >> leg_ik_knee_jnt.translateX
        leg_ik_pv_ctrl_pin_blend.outputG >> leg_ik_ankle_jnt.translateX

        # --------------------------------------------------------------------------------------------------------------
        # create fk joints
        # --------------------------------------------------------------------------------------------------------------
        leg_fk_hip_jnt = xrig.base.joint.Joint('{}_fk_hip_jnt'.format(self.name),
                                               worldMatrix=self.leg_hip.worldMatrix)
        leg_fk_knee_jnt = xrig.base.joint.Joint('{}_fk_knee_jnt'.format(self.name),
                                                worldMatrix=self.leg_knee.worldMatrix, parent=leg_fk_hip_jnt)
        leg_fk_ankle_jnt = xrig.base.joint.Joint('{}_fk_ankle_jnt'.format(self.name),
                                                 worldMatrix=self.leg_ankle.worldMatrix, parent=leg_fk_knee_jnt)

        leg_fk_jnts = [leg_fk_hip_jnt, leg_fk_knee_jnt, leg_fk_ankle_jnt]
        leg_fk_hip_jnt.orient('xyz', 'xup', True, True)

        # --------------------------------------------------------------------------------------------------------------
        # create fk controls
        # --------------------------------------------------------------------------------------------------------------
        leg_fk_ctrls = []
        for leg_fk_jnt in leg_fk_jnts:
            leg_fk_ctrl = Control(leg_fk_jnt.name.replace('_jnt', '_ctrl'),
                                  worldMatrix=leg_fk_jnt.worldMatrix,
                                  rotateOrder='zxy')
            leg_fk_ctrl.rotate = (0.0, 0.0, 0.0)
            leg_fk_ctrl.create_null_grps()
            leg_fk_ctrls.append(leg_fk_ctrl)

            if leg_fk_ctrls.index(leg_fk_ctrl) > 0:
                leg_fk_ctrl.null_grp.parent = leg_fk_ctrls[leg_fk_ctrls.index(leg_fk_ctrl) - 1]

            cmds.pointConstraint(leg_fk_ctrl, leg_fk_jnt, maintainOffset=True)
            cmds.orientConstraint(leg_fk_ctrl, leg_fk_jnt, maintainOffset=True)

        # --------------------------------------------------------------------------------------------------------------
        # add fk stretch
        # --------------------------------------------------------------------------------------------------------------
        for leg_fk_ctrl in leg_fk_ctrls:
            cmds.addAttr(leg_fk_ctrl, shortName='stretch', attributeType='float', readable=True, storable=True,
                         keyable=True, min=0, defaultValue=1)

            leg_fk_ctrl_stretch_factor_div = XNode('{}_stretch_factor_div'.format(leg_fk_ctrl),
                                                   node_type='multiplyDivide',
                                                   operation=2)

            leg_fk_ctrl.stretch >> leg_fk_ctrl_stretch_factor_div.input1X

        # --------------------------------------------------------------------------------------------------------------
        # setup ik/fk blending
        # --------------------------------------------------------------------------------------------------------------
        slv_jnts = [leg_result_hip_jnt, leg_result_knee_jnt, leg_result_ankle_jnt]
        fk_jnts = [leg_fk_hip_jnt, leg_fk_knee_jnt, leg_fk_ankle_jnt]
        ik_jnts = [leg_ik_hip_jnt, leg_ik_knee_jnt, leg_ik_ankle_jnt]
        blend_nodes = []

        for src_jnt, ik_jnt, fk_jnt in zip(slv_jnts, ik_jnts, fk_jnts):
            blend_nodes.append(self.setup_leg_chain_blend(src_jnt, ik_jnt, fk_jnt, 'translate'))
            blend_nodes.append(self.setup_leg_chain_blend(src_jnt, ik_jnt, fk_jnt, 'rotate'))

        # --------------------------------------------------------------------------------------------------------------
        # setup ik/fk blending control
        # --------------------------------------------------------------------------------------------------------------
        leg_fk_ik_blend_ctrl = Control('{}_fk_ik_blend_ctrl'.format(self.name),
                                       control_shape='gear')

        # print leg_fk_ik_blend_ctrl.worldMatrix
        # print leg_result_ankle_jnt.worldMatrix

        cmds.addAttr(leg_fk_ik_blend_ctrl,
                     shortName='fk_ik_blend',
                     longName='fk_ik_blend',
                     keyable=True, readable=True, storable=True, min=0, max=1, defaultValue=0)

        for blend_node in blend_nodes:
            leg_fk_ik_blend_ctrl.fk_ik_blend >> blend_node.blender

        leg_fk_ik_blend_ctrl.worldMatrix = leg_result_ankle_jnt.worldMatrix
        leg_fk_ik_blend_ctrl.rotate = (0.0, 0.0, 0.0)
        leg_fk_ik_blend_ctrl.create_null_grps()

        # --------------------------------------------------------------------------------------------------------------
        # group nodes
        # --------------------------------------------------------------------------------------------------------------
        leg_result_hip_jnt.parent = leg_do_not_touch_grp
        leg_ik_hip_jnt.parent = leg_do_not_touch_grp
        leg_fk_hip_jnt.parent = leg_do_not_touch_grp
        leg_ik_handle.parent = leg_do_not_touch_grp
        leg_ik_root_null.parent = leg_do_not_touch_grp

        leg_fk_ctrls[0].null_grp.parent = leg_ctrl_grp
        leg_ik_ctrl.null_grp.parent = leg_ctrl_grp
        leg_fk_ik_blend_ctrl.null_grp.parent = leg_ctrl_grp
        leg_ik_pv_ctrl.null_grp.parent = leg_ctrl_grp

        # --------------------------------------------------------------------------------------------------------------
        #
        # Foot Setup
        #
        # --------------------------------------------------------------------------------------------------------------
        # --------------------------------------------------------------------------------------------------------------
        # setup reverse foot nulls
        # --------------------------------------------------------------------------------------------------------------
        # todo: create a null at the ankle called l_leg_ik_ankle_stretch_calc_null, parent under ball foot roll null,
        #       use that null in the matrix2 calculations

        foot_ik_ankle_null = XNode('{}_ik_ankle_null'.format(self.name), worldMatrix=self.leg_ankle.worldMatrix)

        foot_ik_heel_null = XNode('{}_ik_heel_null'.format(self.name), worldMatrix=self.leg_heel.worldMatrix)

        foot_ik_out_null = XNode('{}_ik_out_null'.format(self.name), worldMatrix=self.leg_out.worldMatrix,
                                 parent=foot_ik_heel_null)

        foot_ik_in_null = XNode('{}_ik_in_null'.format(self.name), worldMatrix=self.leg_in.worldMatrix,
                                parent=foot_ik_out_null)

        foot_ik_toe_null = XNode('{}_ik_toe_null'.format(self.name), worldMatrix=self.leg_toe.worldMatrix,
                                 parent=foot_ik_in_null, rotateOrder=2)

        foot_ik_ball_null = XNode('{}_ik_ball_null'.format(self.name), worldMatrix=self.leg_ball.worldMatrix,
                                  parent=foot_ik_toe_null)

        foot_ik_ankle_stretch_null = XNode('{}_ik_ankle_stretch_null'.format(self.name),
                                           worldMatrix=self.leg_ankle.worldMatrix,
                                           parent=foot_ik_ball_null)

        foot_ik_toe_raise_null = XNode('{}_ik_toe_raise_null'.format(self.name),
                                       worldMatrix=self.leg_ball.worldMatrix,
                                       parent=foot_ik_toe_null)

        leg_ik_handle.parent = foot_ik_ball_null
        foot_ik_ball_handle.parent = foot_ik_ball_null
        foot_ik_toe_handle.parent = foot_ik_toe_raise_null
        foot_ik_heel_null.parent = foot_ik_ankle_null
        foot_ik_ankle_null.parent = leg_do_not_touch_grp

        cmds.pointConstraint(leg_ik_ctrl, foot_ik_ankle_null)
        cmds.orientConstraint(leg_ik_ctrl, foot_ik_ankle_null)

        # --------------------------------------------------------------------------------------------------------------
        # setup reverse foot roll control
        # --------------------------------------------------------------------------------------------------------------
        # todo: change these attr names
        # add attributes
        cmds.addAttr(leg_ik_ctrl, longName='foot_roll_controls', attributeType='enum', enumName='----------')
        cmds.addAttr(leg_ik_ctrl, longName='roll', attributeType='float', keyable=True, min=-90, max=180)
        cmds.addAttr(leg_ik_ctrl, longName='ball_roll_angle', attributeType='float', keyable=True, defaultValue=45)
        cmds.addAttr(leg_ik_ctrl, longName='toe_roll_angle', attributeType='float', keyable=True, defaultValue=70)
        cmds.addAttr(leg_ik_ctrl, longName='side_tilt', attributeType='float', keyable=True, min=-360, max=360)
        cmds.addAttr(leg_ik_ctrl, longName='ball_lean', attributeType='float', keyable=True)
        cmds.addAttr(leg_ik_ctrl, longName='ball_spin', attributeType='float', keyable=True)
        cmds.addAttr(leg_ik_ctrl, longName='toe_lean', attributeType='float', keyable=True)
        cmds.addAttr(leg_ik_ctrl, longName='toe_spin', attributeType='float', keyable=True)
        cmds.addAttr(leg_ik_ctrl, longName='toe_raise', attributeType='float', keyable=True)

        cmds.setAttr('{}.foot_roll_controls'.format(leg_ik_ctrl), channelBox=True)

        # --------------------------------------------------------------------------------------------------------------
        # setup reverse foot roll nodes
        # --------------------------------------------------------------------------------------------------------------
        # heel clamp, heel result
        foot_roll_heel_clamp = XNode('{}_roll_heel_clamp'.format(self.name), node_type='clamp', minR=-90.0)
        leg_ik_ctrl.roll >> foot_roll_heel_clamp.inputR
        foot_roll_heel_clamp.outputR >> foot_ik_heel_null.rotateX

        # ball clamp
        foot_roll_ball_clamp = XNode('{}_roll_ball_clamp'.format(self.name), node_type='clamp')
        leg_ik_ctrl.roll >> foot_roll_ball_clamp.inputR
        leg_ik_ctrl.ball_roll_angle >> foot_roll_ball_clamp.maxR

        # ball normalize
        foot_roll_ball_normalize = XNode('{}_roll_ball_normalize'.format(self.name), node_type='setRange', maxX=1.0)
        foot_roll_ball_clamp.minR >> foot_roll_ball_normalize.oldMinX
        foot_roll_ball_clamp.maxR >> foot_roll_ball_normalize.oldMaxX
        foot_roll_ball_clamp.inputR >> foot_roll_ball_normalize.valueX

        # toe clamp
        foot_roll_toe_clamp = XNode('{}_roll_toe_clamp'.format(self.name), node_type='clamp')
        leg_ik_ctrl.roll >> foot_roll_toe_clamp.inputR
        leg_ik_ctrl.ball_roll_angle >> foot_roll_toe_clamp.minR
        leg_ik_ctrl.toe_roll_angle >> foot_roll_toe_clamp.maxR

        # heel normalize
        foot_roll_heel_normalize = XNode('{}_roll_heel_normalize'.format(self.name), node_type='setRange', maxX=1.0)
        foot_roll_toe_clamp.minR >> foot_roll_heel_normalize.oldMinX
        foot_roll_toe_clamp.maxR >> foot_roll_heel_normalize.oldMaxX
        foot_roll_toe_clamp.inputR >> foot_roll_heel_normalize.valueX

        # toe result
        foot_roll_toe_result_mult = XNode('{}_roll_toe_result_mult'.format(self.name), node_type='multiplyDivide')
        foot_roll_heel_normalize.outValueX >> foot_roll_toe_result_mult.input1X
        foot_roll_heel_clamp.inputR >> foot_roll_toe_result_mult.input2X
        foot_roll_toe_result_mult.outputX >> foot_ik_toe_null.rotateX

        # ball invert
        foot_roll_ball_invert = XNode('{}_roll_ball_invert'.format(self.name), node_type='plusMinusAverage',
                                      operation=2)
        cmds.setAttr('{}.input1D[0]'.format(foot_roll_ball_invert), 1.0)
        cmds.setAttr('{}.input1D[1]'.format(foot_roll_ball_invert), 1.0)
        # foot_roll_heel_normalize.outValueX >> foot_roll_ball_invert.input1D
        foot_roll_heel_normalize.connect('outValueX', foot_roll_ball_invert, 'input1D[1]')

        # ball invert mult
        foot_roll_ball_invert_mult = XNode('{}_roll_ball_invert_mult'.format(self.name), node_type='multiplyDivide')
        foot_roll_ball_normalize.outValueX >> foot_roll_ball_invert_mult.input1X
        foot_roll_ball_invert.output1D >> foot_roll_ball_invert_mult.input2X

        # ball result
        foot_roll_ball_result_mult = XNode('{}_roll_ball_result_mult'.format(self.name), node_type='multiplyDivide')
        foot_roll_ball_invert_mult.outputX >> foot_roll_ball_result_mult.input1X
        foot_roll_ball_result_mult.outputX >> foot_ik_ball_null.rotateX
        leg_ik_ctrl.roll >> foot_roll_ball_result_mult.input2X

        # --------------------------------------------------------------------------------------------------------------
        # connect reverse foot roll attributes
        # --------------------------------------------------------------------------------------------------------------
        leg_ik_ctrl.ball_spin >> foot_ik_ball_null.rotateY
        leg_ik_ctrl.ball_lean >> foot_ik_ball_null.rotateZ

        leg_ik_ctrl.toe_spin >> foot_ik_toe_null.rotateY
        leg_ik_ctrl.toe_lean >> foot_ik_toe_null.rotateZ

        leg_ik_ctrl.toe_raise >> foot_ik_toe_raise_null.rotateX

        # --------------------------------------------------------------------------------------------------------------
        # setup reverse foot roll side tilt
        # --------------------------------------------------------------------------------------------------------------
        foot_roll_tilt_in_remap = XNode('{}_roll_tilt_in_remap'.format(self.name), node_type='remapValue',
                                        inputMax=360, outputMax=360)
        leg_ik_ctrl.side_tilt >> foot_roll_tilt_in_remap.inputValue
        foot_roll_tilt_in_remap.outColorR >> foot_ik_in_null.rotateZ

        foot_roll_tilt_out_remap = XNode('{}_roll_tilt_out_remap'.format(self.name), node_type='remapValue',
                                         inputMin=-360, outputMin=-360)
        leg_ik_ctrl.side_tilt >> foot_roll_tilt_out_remap.inputValue
        foot_roll_tilt_out_remap.outColorR >> foot_ik_out_null.rotateZ

        # --------------------------------------------------------------------------------------------------------------
        # override previous stretch calculation nodes with new foot roll ankle node
        # --------------------------------------------------------------------------------------------------------------
        foot_ik_ankle_stretch_null.worldMatrix >> leg_ik_ctrl_stretch_ctrl_dist.inMatrix2
        foot_ik_ankle_stretch_null.worldMatrix >> leg_ik_pv_ctrl_pin_tip_dist.inMatrix2

    @staticmethod
    def setup_leg_chain_blend(src_jnt, ik_jnt, fk_jnt, attr):
        blend_node = XNode('{}_{}_blend'.format(src_jnt, attr), node_type='blendColors')
        ik_jnt.connect(attr, blend_node, 'color1')
        fk_jnt.connect(attr, blend_node, 'color2')
        blend_node.connect('output', src_jnt, attr)
        return blend_node

    @staticmethod
    def calculate_pv_ctrl_pos(root_jnt, mid_jnt, tip_jnt, vector_scale=1.0):
        root_vec = xrig.util.maya_api.create_mvector(root_jnt)
        mid_vec = xrig.util.maya_api.create_mvector(mid_jnt)
        tip_vec = xrig.util.maya_api.create_mvector(tip_jnt)

        root_to_tip_vec = tip_vec - root_vec
        root_to_mid_vec = mid_vec - root_vec

        dot_prod = root_to_mid_vec * root_to_tip_vec
        projected_length = float(dot_prod) / float(root_to_tip_vec.length())
        norm_root_to_tip_vec = root_to_tip_vec.normal()
        projected_vec = norm_root_to_tip_vec * projected_length
        arrow_vec = root_to_mid_vec - projected_vec
        arrow_vec *= vector_scale

        result_vec = arrow_vec + mid_vec
        return result_vec.x, result_vec.y, result_vec.z

    def hook(self, spine_limb):
        pass

        """
        1. ik jnts and measurement locs into a group
        2. slave jnts into a group
        3. set groups pivots to the thigh jnt
        4. l_leg_hip_space_loc at thigh jnt parent under hip bind jnt (result)
        5. point contstrain groups 1, 2 to loc
        
        6. do same for fk chain
        7. add rotation space switching for hip fk ctrl: hip, upperbody, root
        
        8. add a fix to fix the orientation of the ik ctrl (part 18: 7:30)
        
        9. root ctrl ik scale fix
        
        """
