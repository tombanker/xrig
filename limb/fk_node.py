"""
input: root_pos, tip_pos, mid_pos? inbetween_joints
output:

"""
import maya.cmds as cmds

import xrig.base.joint
import xrig.base.limb
import xrig.util.maya_api
from xrig.base.control import Control
from xrig.base.xnode import XNode


class FKNode(xrig.base.limb.Limb):
    def __init__(self, name, root, prefix='', limb=''):
        super(FKNode, self).__init__(name=name)
        self.prefix = prefix
        self.limb = limb
        self.name = '{}{}_fk_{}'.format(self.prefix, self.limb, self.name)
        self.root = XNode(root)
        self.fk_jnt = None
        self.fk_ctrl = None
        self.create()

    def create(self):
        self.fk_jnt = xrig.base.joint.Joint(name='{}_jnt'.format(self.name), worldMatrix=self.root.worldMatrix)
        self.fk_ctrl = xrig.base.control.Control(name='{}_ctrl'.format(self.name),
                                                 worldMatrix=self.fk_jnt.worldMatrix)

        cmds.pointConstraint(self.fk_ctrl, self.fk_jnt)
        cmds.orientConstraint(self.fk_ctrl, self.fk_jnt)

        self.fk_ctrl.create_null_grps()

    def hook(self):
        pass


"""
leg_fk_chain = [leg_fk_hip, leg_fk_knee, leg_fk_ankle]



"""
