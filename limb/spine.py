import xrig.base.xform
import xrig.base.joint_old
import xrig.base.control_old
import xrig.base.limb
import xrig.base.xnode
import xrig.base.joint
import xrig.base.control
import xrig.limb.fk_limb
import maya.cmds as cmds


# todo: make a template object w/ start and end joints, optionally any number of inbetween joints
# todo: better naming convention for joints, use same naming convention for variable names
# todo: create a custom named joint duplicate for start/end joint and use that instead
# todo: plugin the global scale ('spine_grp') into the squash/stretch calculations
# todo: add rotate order dict to xnode

class Spine(xrig.base.limb.Limb):
    def __init__(self, spine_start, spine_end, num_mid_jnts=5):
        super(Spine, self).__init__(name='spine')
        self.spine_start = xrig.base.xnode.XNode(spine_start)
        self.spine_end = xrig.base.xnode.XNode(spine_end)
        self.num_mid_jnts = num_mid_jnts

    def create(self):
        # --------------------------------------------------------------------------------------------------------------
        # create groups
        # --------------------------------------------------------------------------------------------------------------
        spine_grp = xrig.base.xnode.XNode(name='{}_grp'.format(self.name))
        spine_ctrl_grp = xrig.base.xnode.XNode(name='{}_ctrl_grp'.format(self.name), parent=spine_grp)
        self.spine_do_not_touch_grp = xrig.base.xnode.XNode(name='{}_do_not_touch_grp'.format(self.name),
                                                            parent=spine_grp)

        # --------------------------------------------------------------------------------------------------------------
        # create ik joints
        # --------------------------------------------------------------------------------------------------------------
        spine_start_jnt = xrig.base.joint.Joint('spine_ik_00_jnt', worldMatrix=self.spine_start.worldMatrix)
        spine_end_jnt = xrig.base.joint.Joint('spine_ik_{}_jnt'.format(str(self.num_mid_jnts + 1).rjust(2, '0')),
                                              worldMatrix=self.spine_end.worldMatrix)

        xrig.base.joint.insert_joints(start_joint=spine_start_jnt,
                                      end_joint=spine_end_jnt,
                                      num_mid_joints=self.num_mid_jnts,
                                      prefix='spine_ik')

        # --------------------------------------------------------------------------------------------------------------
        # setup ik spline
        # --------------------------------------------------------------------------------------------------------------
        # set the chain orientation
        spine_start_jnt.orient('xyz', 'zup', True, True)

        __ik_handle = cmds.ikHandle(name='spine_ik_handle',
                                    startJoint=spine_start_jnt,
                                    endEffector=spine_end_jnt,
                                    solver='ikSplineSolver')

        spine_ik_handle = xrig.base.xnode.XNode(__ik_handle[0])
        spine_ik_handle_eff = xrig.base.xnode.XNode(__ik_handle[1])
        spine_ik_handle_crv = xrig.base.xnode.XNode(__ik_handle[2])
        spine_ik_handle_eff.name = 'spine_ik_handle_eff'
        spine_ik_handle_crv.name = 'spine_ik_handle_crv'

        # create two joints and skin them to the spline curve
        spine_ik_hip_crv_jnt = xrig.base.joint.Joint('spine_ik_hip_ctrl_jnt',
                                                     rotateOrder=2,
                                                     jointOrient=spine_start_jnt.jointOrient)
        spine_ik_hip_crv_jnt.worldMatrix = spine_start_jnt.worldMatrix

        self.spine_ik_chest_crv_jnt = xrig.base.joint.Joint('spine_ik_chest_ctrl_jnt',
                                                            rotateOrder=2,
                                                            jointOrient=spine_end_jnt.jointOrient)
        self.spine_ik_chest_crv_jnt.worldMatrix = spine_end_jnt.worldMatrix
        cmds.skinCluster(spine_ik_hip_crv_jnt.name, self.spine_ik_chest_crv_jnt.name, spine_ik_handle_crv.name)

        # --------------------------------------------------------------------------------------------------------------
        # create ik spine controls
        # --------------------------------------------------------------------------------------------------------------
        spine_ik_hip_ctrl = xrig.base.control.Control(name='spine_ik_hip_ctrl',
                                                      control_shape='cube',
                                                      rotateOrder='zxy')
        spine_ik_hip_ctrl.create_null_grps()
        spine_ik_hip_ctrl.null_grp.worldMatrix = spine_start_jnt.worldMatrix

        spine_ik_chest_ctrl = xrig.base.control.Control(name='spine_ik_chest_ctrl',
                                                        control_shape='cube',
                                                        rotateOrder='zxy')
        spine_ik_chest_ctrl.create_null_grps()
        spine_ik_chest_ctrl.null_grp.worldMatrix = spine_end_jnt.worldMatrix

        # parent constrain controls to control joints
        cmds.parentConstraint(spine_ik_hip_ctrl.name, spine_ik_hip_crv_jnt, maintainOffset=True)
        cmds.parentConstraint(spine_ik_chest_ctrl.name, self.spine_ik_chest_crv_jnt, maintainOffset=True)

        # add ik spline handle advanced twist
        spine_ik_handle.dTwistControlEnable = 1
        spine_ik_handle.dWorldUpType = 4
        spine_ik_handle.dForwardAxis = 0
        spine_ik_handle.dWorldUpAxis = 3
        spine_ik_handle.dWorldUpVector = (0, 0, 1)
        spine_ik_handle.dWorldUpVectorEnd = (0, 0, 1)

        spine_ik_hip_crv_jnt.worldMatrix >> spine_ik_handle.dWorldUpMatrix
        self.spine_ik_chest_crv_jnt.worldMatrix >> spine_ik_handle.dWorldUpMatrixEnd
        # --------------------------------------------------------------------------------------------------------------
        # setup fk spine chain
        # --------------------------------------------------------------------------------------------------------------
        spine_hip_fk_jnt = xrig.base.joint.Joint(name='spine_hip_fk_jnt',
                                                 jointOrient=spine_start_jnt.jointOrient,
                                                 worldMatrix=spine_start_jnt.worldMatrix)

        spine_chest_fk_jnt = xrig.base.joint.Joint(name='spine_chest_fk_jnt',
                                                   jointOrient=spine_end_jnt.jointOrient,
                                                   worldMatrix=spine_end_jnt.worldMatrix)

        spine_fk_jnts = xrig.base.joint.insert_joints(start_joint=spine_hip_fk_jnt,
                                                      end_joint=spine_chest_fk_jnt,
                                                      num_mid_joints=2,
                                                      prefix='spine_fk')

        # set the chain orientation
        spine_hip_fk_jnt.freeze()
        spine_hip_fk_jnt.orient('xyz', 'zup', True, True)

        for jnt in spine_fk_jnts:
            jnt.rotateOrder = 1

        cmds.parentConstraint(spine_hip_fk_jnt, spine_ik_hip_ctrl.const_grp, maintainOffset=True)
        cmds.parentConstraint(spine_chest_fk_jnt, spine_ik_chest_ctrl.const_grp, maintainOffset=True)

        # --------------------------------------------------------------------------------------------------------------
        # create fk spine controls
        # --------------------------------------------------------------------------------------------------------------
        spine_fk_hip_ctrl = xrig.base.control.Control(name='spine_fk_hip_ctrl',
                                                      control_shape='circle',
                                                      rotateOrder='zxy',
                                                      wm=spine_fk_jnts[0].wm,
                                                      r=spine_fk_jnts[0].jointOrient)
        spine_fk_hip_ctrl.create_null_grps()

        spine_fk_chest_ctrl = xrig.base.control.Control(name='spine_fk_chest_ctrl',
                                                        control_shape='circle',
                                                        rotateOrder='zxy',
                                                        wm=spine_fk_jnts[1].wm,
                                                        r=spine_fk_jnts[1].jointOrient)
        spine_fk_chest_ctrl.create_null_grps()

        spine_fk_chest_ctrl.null_grp.parent = spine_fk_hip_ctrl
        cmds.parentConstraint(spine_fk_hip_ctrl.name, spine_fk_jnts[0], maintainOffset=True)
        cmds.parentConstraint(spine_fk_chest_ctrl.name, spine_fk_jnts[1], maintainOffset=True)
        cmds.parentConstraint(spine_hip_fk_jnt.name, spine_fk_hip_ctrl.const_grp.name, maintainOffset=True)

        # --------------------------------------------------------------------------------------------------------------
        # create stretchy spine utility nodes
        # --------------------------------------------------------------------------------------------------------------
        spine_curve_info = xrig.base.xnode.XNode('spine_curve_info', node_type='curveInfo')
        # todo: figure this out
        # spine_ik_handle_crv.worldSpace[0] >> spine_curve_info.inputCurve
        spine_ik_handle_crv.connect('worldSpace[0]', spine_curve_info, 'inputCurve')

        spine_stretch_factor_div = xrig.base.xnode.XNode('spine_stretch_factor_div',
                                                         node_type='multiplyDivide',
                                                         operation=2)

        spine_curve_info.arcLength >> spine_stretch_factor_div.input1X
        # spine_curve_info.connect('arcLength', spine_stretch_factor_div, 'input1X')
        spine_stretch_factor_div.input2X = spine_stretch_factor_div.input1X

        # connect md output into scaleX value of each spine1_result_jnt
        for jnt in spine_start_jnt.chain:
            spine_stretch_factor_div.outputX >> jnt.scaleX
            # spine_stretch_factor_div.connect('outputX', jnt, 'scaleX')

        # --------------------------------------------------------------------------------------------------------------
        # create stretchy spine volume preservation utility nodes
        # --------------------------------------------------------------------------------------------------------------
        spine_stretch_sqrt_pow = xrig.base.xnode.XNode('spine_stretch_sqrt_pow',
                                                       node_type='multiplyDivide',
                                                       operation=3,
                                                       input2X=0.5)
        # spine_stretch_factor_div.connect('outputX', spine_stretch_sqrt_pow, 'input1X')
        spine_stretch_factor_div.outputX >> spine_stretch_sqrt_pow.input1X

        spine_stretch_invert_div = xrig.base.xnode.XNode('spine_stretch_invert_div',
                                                         node_type='multiplyDivide',
                                                         operation=2,
                                                         input1X=1)
        # spine_stretch_sqrt_pow.connect('outputX', spine_stretch_invert_div, 'input2X')
        spine_stretch_sqrt_pow.outputX >> spine_stretch_invert_div.input2X

        for start_joint_child in spine_start_jnt.chain:
            # spine_stretch_invert_div.connect('outputX', start_joint_child, 'scaleY')
            # spine_stretch_invert_div.connect('outputX', start_joint_child, 'scaleZ')
            spine_stretch_invert_div.outputX >> start_joint_child.scaleY
            spine_stretch_invert_div.outputX >> start_joint_child.scaleZ

        # --------------------------------------------------------------------------------------------------------------
        # create global spine control
        # --------------------------------------------------------------------------------------------------------------
        self.spine_body_ctrl = xrig.base.control.Control(name='spine_body_ctrl',
                                                         control_shape='square',
                                                         worldMatrix=spine_start_jnt.worldMatrix)
        self.spine_body_ctrl.create_null_grps()
        cmds.parentConstraint(self.spine_body_ctrl.name, spine_hip_fk_jnt.name, maintainOffset=True)

        # --------------------------------------------------------------------------------------------------------------
        # fix global scale offset in spline curve info arc length
        # --------------------------------------------------------------------------------------------------------------
        spine_global_scale_normalize_div = xrig.base.xnode.XNode('spine_global_scale_normalize_div',
                                                                 node_type='multiplyDivide',
                                                                 operation=2)

        spine_ik_handle_crv.inheritsTransform = 0

        # spine_curve_info.connect('arcLength', spine_global_scale_normalize_div, 'input1X')
        # spine_grp.connect('scaleY', spine_global_scale_normalize_div, 'input2X')
        # spine_curve_info.connect('arcLength', spine_global_scale_normalize_div, 'input1X')
        # spine_global_scale_normalize_div.connect('outputX', spine_stretch_factor_div, 'input1X')
        spine_curve_info.arcLength >> spine_global_scale_normalize_div.input1X
        spine_grp.scaleY >> spine_global_scale_normalize_div.input2X
        # spine_curve_info.arcLength >> spine_global_scale_normalize_div.input1X
        # spine_global_scale_normalize_div.outputX >> spine_stretch_factor_div.input1X

        # --------------------------------------------------------------------------------------------------------------
        # group nodes
        # --------------------------------------------------------------------------------------------------------------
        spine_start_jnt.parent = self.spine_do_not_touch_grp
        spine_ik_hip_crv_jnt.parent = self.spine_do_not_touch_grp
        self.spine_ik_chest_crv_jnt.parent = self.spine_do_not_touch_grp
        spine_ik_handle.parent = self.spine_do_not_touch_grp
        spine_ik_handle_crv.parent = self.spine_do_not_touch_grp
        spine_hip_fk_jnt.parent = self.spine_do_not_touch_grp

        self.spine_body_ctrl.null_grp.parent = spine_ctrl_grp
        spine_fk_hip_ctrl.null_grp.parent = spine_ctrl_grp
        spine_ik_hip_ctrl.null_grp.parent = spine_ctrl_grp
        spine_ik_chest_ctrl.null_grp.parent = spine_ctrl_grp

        # --------------------------------------------------------------------------------------------------------------
        # lock and hide attributes
        # --------------------------------------------------------------------------------------------------------------
        # # self.spine_ik_chest_ctrl.lock_and_hide_attr('s')
        # # self.spine_ik_hip_ctrl.lock_and_hide_attr('s')
        # # self.spine_fk_chest_ctrl.lock_and_hide_attr('s')
        # # self.spine_fk_hip_ctrl.lock_and_hide_attr('s')
        return True

    def hook(self):
        pass
