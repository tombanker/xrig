"""
input: root_pos, tip_pos, mid_pos? inbetween_joints
output:

"""
import xrig.base.limb
import xrig.util.maya_api


class FkChain(xrig.base.limb.Limb):
    def __init__(self, chain):
        self.chain = chain
        self.name = self.chain[0].name
        super(FkChain, self).__init__(name=self.name)

        self.fk_jnts = [chain_null.fk_jnt for chain_null in self.chain]
        self.fk_ctrls = [chain_null.fk_ctrl for chain_null in self.chain]

        self.create()

    def create(self):
        for index, jnt in enumerate(self.fk_jnts):
            if index > 0:
                self.fk_jnts[index].parent = self.fk_jnts[index - 1]

        for index, jnt in enumerate(self.fk_jnts):
            if index > 0:
                self.fk_ctrls[index].null_grp.parent = self.fk_ctrls[index - 1]

    def hook(self):
        pass
