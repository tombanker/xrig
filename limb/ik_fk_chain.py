"""
Module Docstring
"""

__author__ = 'tbanker'
__version__ = "1.0.0"

from xrig.base.xnode import XNode
from xrig.base.joint import Joint
import xrig.base.joint
from xrig.base.control import Control
import xrig.base.limb
import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
import xrig.util.maya_api


class IkFkChain(xrig.base.limb.Limb):
    def __init__(self, name, base, mid, tip, prefix=''):
        super(IkFkChain, self).__init__(name='ikfkchain')
        self.name = name
        self.prefix = prefix
        self.name = '{}{}'.format(self.prefix, self.name)

        self.limb_base = XNode(name=base)
        self.limb_mid = XNode(name=mid)
        self.limb_tip = XNode(name=tip)

        self.pivot_nodes = [self.limb_base, self.limb_mid, self.limb_tip]

    def create(self):
        # --------------------------------------------------------------------------------------------------------------
        # create groups
        # --------------------------------------------------------------------------------------------------------------
        limb_grp = XNode('{}_grp'.format(self.name))
        limb_ctrl_grp = XNode('{}_ctrl_grp'.format(self.name), parent=limb_grp)
        limb_do_not_touch_grp = XNode('{}_do_not_touch_grp'.format(self.name), parent=limb_grp)

        # --------------------------------------------------------------------------------------------------------------
        # create result joints
        # --------------------------------------------------------------------------------------------------------------
        limb_result_base_jnt = Joint('{}_result_base_jnt'.format(self.name), worldMatrix=self.limb_base.worldMatrix)
        limb_result_mid_jnt = Joint('{}_result_mid_jnt'.format(self.name), worldMatrix=self.limb_mid.worldMatrix,
                                    parent=limb_result_base_jnt)
        limb_result_tip_jnt = Joint('{}_result_tip_jnt'.format(self.name), worldMatrix=self.limb_tip.worldMatrix,
                                    parent=limb_result_mid_jnt)
        limb_result_base_jnt.orient('xyz', 'xup', True, True)

        # --------------------------------------------------------------------------------------------------------------
        # create ik joints
        # --------------------------------------------------------------------------------------------------------------
        limb_ik_base_jnt = Joint('{}_ik_base_jnt'.format(self.name), worldMatrix=self.limb_base.worldMatrix)
        limb_ik_mid_jnt = Joint('{}_ik_mid_jnt'.format(self.name), worldMatrix=self.limb_mid.worldMatrix,
                                parent=limb_ik_base_jnt)
        limb_ik_tip_jnt = Joint('{}_ik_tip_jnt'.format(self.name), worldMatrix=self.limb_tip.worldMatrix,
                                parent=limb_ik_mid_jnt)
        limb_ik_base_jnt.orient('xyz', 'xup', True, True)

        # --------------------------------------------------------------------------------------------------------------
        # setup ik handles and control
        # --------------------------------------------------------------------------------------------------------------
        __ik_handle = cmds.ikHandle(name='{}_ik_handle'.format(self.name))
        limb_ik_handle, limb_ik_handle_eff = XNode(__ik_handle[0]), XNode(__ik_handle[1])
        limb_ik_handle_eff.name = '{}_ik_handle_eff'.format(self.name)

        limb_ik_ctrl = Control('{}_ik_ctrl'.format(self.name), control_shape='cube',
                               worldMatrix=self.limb_tip.worldMatrix, rotateOrder='zxy')

        limb_ik_ctrl.rotate = (0.0, 0.0, 0.0)
        limb_ik_ctrl.create_null_grps()

        # add attributes
        cmds.addAttr(limb_ik_ctrl, longName='ik_chain_controls', attributeType='enum', enumName='----------',
                     keyable=False)
        cmds.addAttr(limb_ik_ctrl, longName='stretch', attributeType='float', keyable=True, min=0, max=1,
                     defaultValue=1)
        cmds.addAttr(limb_ik_ctrl, longName='mid_pin', attributeType='float', keyable=True, min=0, max=1,
                     defaultValue=0)
        cmds.addAttr(limb_ik_ctrl, longName='mid_twist', attributeType='float', keyable=True)
        cmds.addAttr(limb_ik_ctrl, longName='stretch_amount', attributeType='float', keyable=False)

        cmds.setAttr('{}.ik_chain_controls'.format(limb_ik_ctrl), channelBox=True)
        cmds.setAttr('{}.stretch_amount'.format(limb_ik_ctrl), channelBox=True)

        # --------------------------------------------------------------------------------------------------------------
        # setup ik stretching
        # --------------------------------------------------------------------------------------------------------------
        # create a null object for the ik root joint
        limb_ik_root_null = XNode('{}_ik_root_null'.format(self.name),
                                  worldMatrix=limb_ik_base_jnt.worldMatrix)

        # get distance between ik root and mid jnt
        limb_ik_ctrl_stretch_mid_dist = XNode('{}_stretch_mid_dist'.format(limb_ik_ctrl),
                                              node_type='distanceBetween')
        limb_ik_root_null.worldMatrix >> limb_ik_ctrl_stretch_mid_dist.inMatrix1
        limb_ik_mid_jnt.worldMatrix >> limb_ik_ctrl_stretch_mid_dist.inMatrix2

        # get distance between ik mid and end jnt
        limb_ik_ctrl_stretch_tip_dist = XNode('{}_stretch_tip_dist'.format(limb_ik_ctrl),
                                              node_type='distanceBetween')
        limb_ik_mid_jnt.worldMatrix >> limb_ik_ctrl_stretch_tip_dist.inMatrix1
        limb_ik_tip_jnt.worldMatrix >> limb_ik_ctrl_stretch_tip_dist.inMatrix2

        # get distance between ik root and ik handle
        limb_ik_ctrl_stretch_ctrl_dist = XNode('{}_stretch_ctrl_dist'.format(limb_ik_ctrl),
                                               node_type='distanceBetween')
        limb_ik_root_null.worldMatrix >> limb_ik_ctrl_stretch_ctrl_dist.inMatrix1
        limb_ik_ctrl.worldMatrix >> limb_ik_ctrl_stretch_ctrl_dist.inMatrix2

        # input the distance between ik root and ik handle; and total chain distance into a multiplyDivide node
        limb_ik_ctrl_stretch_factor_div = XNode('{}_stretch_factor_div'.format(limb_ik_ctrl),
                                                node_type='multiplyDivide',
                                                operation=2)
        limb_ik_ctrl_stretch_ctrl_dist.distance >> limb_ik_ctrl_stretch_factor_div.input1X
        limb_ik_ctrl_stretch_sum_dist = float(limb_ik_ctrl_stretch_mid_dist.distance.value +
                                              limb_ik_ctrl_stretch_tip_dist.distance.value)
        limb_ik_ctrl_stretch_factor_div.input2X = limb_ik_ctrl_stretch_sum_dist

        # create a condition node to not stretch if the distance between joints are shorter than the rest length
        limb_ik_ctrl_stretch_cond = XNode('{}_stretch_cond'.format(limb_ik_ctrl),
                                          node_type='condition',
                                          operation=2,
                                          secondTerm=limb_ik_ctrl_stretch_sum_dist)
        limb_ik_ctrl_stretch_ctrl_dist.distance >> limb_ik_ctrl_stretch_cond.firstTerm
        limb_ik_ctrl_stretch_factor_div.outputX >> limb_ik_ctrl_stretch_cond.colorIfTrueR
        limb_ik_ctrl_stretch_factor_div.outputX >> limb_ik_ctrl.stretch_amount

        # create a stretch factor multiply divide for the mid ik joint
        limb_ik_ctrl_stretch_mid_mult = XNode('{}_stretch_mid_mult'.format(limb_ik_ctrl),
                                              node_type='multiplyDivide',
                                              input2X=limb_ik_ctrl_stretch_mid_dist.distance)
        limb_ik_ctrl_stretch_cond.outColorR >> limb_ik_ctrl_stretch_mid_mult.input1X
        # create a stretch factor multiply divide for the end ik joint
        limb_ik_ctrl_stretch_tip_mult = XNode('{}_stretch_tip_mult'.format(limb_ik_ctrl),
                                              node_type='multiplyDivide',
                                              input2X=limb_ik_ctrl_stretch_tip_dist.distance)
        limb_ik_ctrl_stretch_cond.outColorR >> limb_ik_ctrl_stretch_tip_mult.input1X

        # create a blend colors node to blend between stretching or not stretching
        limb_ik_ctrl_stretch_blend = XNode('{}_stretch_blend'.format(limb_ik_ctrl),
                                           node_type='blendColors',
                                           color2R=limb_ik_ctrl_stretch_mid_dist.distance,
                                           color2G=limb_ik_ctrl_stretch_tip_dist.distance)
        limb_ik_ctrl.stretch >> limb_ik_ctrl_stretch_blend.blender
        limb_ik_ctrl_stretch_mid_mult.outputX >> limb_ik_ctrl_stretch_blend.color1R
        limb_ik_ctrl_stretch_tip_mult.outputX >> limb_ik_ctrl_stretch_blend.color1G

        # --------------------------------------------------------------------------------------------------------------
        # setup mid pole vector
        # --------------------------------------------------------------------------------------------------------------
        limb_ik_pv_ctrl = Control('{}_ik_pv_ctrl'.format(self.name),
                                  control_shape='half_diamond',
                                  worldMatrix=limb_ik_mid_jnt.worldMatrix)
        limb_ik_pv_ctrl.rotate = (0.0, 0.0, 0.0)

        limb_ik_pv_ctrl.translate = self.calculate_pv_ctrl_pos(limb_ik_base_jnt, limb_ik_mid_jnt, limb_ik_tip_jnt, 10.0)
        cmds.poleVectorConstraint(limb_ik_pv_ctrl, limb_ik_handle)
        limb_ik_pv_ctrl.create_null_grps()

        # --------------------------------------------------------------------------------------------------------------
        # setup no flip mid
        # --------------------------------------------------------------------------------------------------------------
        # limb_ik_pv_no_flip_ctrl = Control('{}_ik_pv_no_flip_ctrl'.format(self.name),
        #                                  worldMatrix=limb_ik_tip_jnt.worldMatrix)
        # limb_ik_pv_no_flip_ctrl.rotate = (0.0, 0.0, 0.0)
        # limb_ik_pv_no_flip_ctrl.translateX += 100
        # limb_ik_pv_no_flip_ctrl.create_null_grps()
        # limb_ik_pv_no_flip_ctrl.anim_grp.translate = limb_ik_pv_no_flip_ctrl.null_grp.translate
        # limb_ik_pv_no_flip_ctrl.null_grp.translate = (0.0, 0.0, 0.0)
        #
        # cmds.poleVectorConstraint(limb_ik_pv_no_flip_ctrl, limb_ik_handle)
        # limb_ik_handle.twist = 90
        # limb_ik_ctrl.mid_twist >> limb_ik_pv_no_flip_ctrl.ll_grp'rotateY')

        # --------------------------------------------------------------------------------------------------------------
        # setup dual no flip mid
        # --------------------------------------------------------------------------------------------------------------
        # duplicate ik chain twice (function?)
        # duplicate special

        # --------------------------------------------------------------------------------------------------------------
        # setup mid pinning
        # --------------------------------------------------------------------------------------------------------------
        limb_ik_pv_ctrl_pin_root_dist = XNode('{}_pin_root_dist'.format(limb_ik_pv_ctrl),
                                              node_type='distanceBetween')
        limb_ik_root_null.worldMatrix >> limb_ik_pv_ctrl_pin_root_dist.inMatrix1
        limb_ik_pv_ctrl.worldMatrix >> limb_ik_pv_ctrl_pin_root_dist.inMatrix2

        limb_ik_pv_ctrl_pin_tip_dist = XNode('{}_pin_tip_dist'.format(limb_ik_pv_ctrl),
                                             node_type='distanceBetween')
        limb_ik_pv_ctrl.worldMatrix >> limb_ik_pv_ctrl_pin_tip_dist.inMatrix1
        # todo: note and test this change - reorder foot ik handle creation to higher up
        # limb_ik_handle_null = XNode('{}_null'.format(limb_ik_handle), worldMatrix=limb_ik_handle.worldMatrix,
        #                            parent=limb_ik_handle)
        limb_ik_ctrl.worldMatrix >> limb_ik_pv_ctrl_pin_tip_dist.inMatrix2
        # limb_ik_handle.worldMatrix >> limb_ik_pv_ctrl_pin_tip_dist.inMatrix2
        # limb_ik_handle_null.worldMatrix >> limb_ik_pv_ctrl_pin_tip_dist.inMatrix2
        # limb_ik_handle_null.worldMatrix >> limb_ik_pv_ctrl_pin_tip_dist.inMatrix2

        # create a blend colors node to blend between mid pinning
        limb_ik_pv_ctrl_pin_blend = XNode('{}_pin_blend'.format(limb_ik_pv_ctrl),
                                          node_type='blendColors',
                                          color2R=limb_ik_pv_ctrl_pin_root_dist.distance,
                                          color2G=limb_ik_pv_ctrl_pin_tip_dist.distance)

        limb_ik_ctrl.mid_pin >> limb_ik_pv_ctrl_pin_blend.blender
        limb_ik_pv_ctrl_pin_root_dist.distance >> limb_ik_pv_ctrl_pin_blend.color1R
        limb_ik_pv_ctrl_pin_tip_dist.distance >> limb_ik_pv_ctrl_pin_blend.color1G
        limb_ik_ctrl_stretch_blend.outputR >> limb_ik_pv_ctrl_pin_blend.color2R
        limb_ik_ctrl_stretch_blend.outputG >> limb_ik_pv_ctrl_pin_blend.color2G
        limb_ik_pv_ctrl_pin_blend.outputR >> limb_ik_mid_jnt.translateX
        limb_ik_pv_ctrl_pin_blend.outputG >> limb_ik_tip_jnt.translateX

        # --------------------------------------------------------------------------------------------------------------
        # create fk joints
        # --------------------------------------------------------------------------------------------------------------
        limb_fk_base_jnt = xrig.base.joint.Joint('{}_fk_base_jnt'.format(self.name),
                                                 worldMatrix=self.limb_base.worldMatrix)
        limb_fk_mid_jnt = xrig.base.joint.Joint('{}_fk_mid_jnt'.format(self.name),
                                                worldMatrix=self.limb_mid.worldMatrix, parent=limb_fk_base_jnt)
        limb_fk_tip_jnt = xrig.base.joint.Joint('{}_fk_tip_jnt'.format(self.name),
                                                worldMatrix=self.limb_tip.worldMatrix, parent=limb_fk_mid_jnt)

        limb_fk_jnts = [limb_fk_base_jnt, limb_fk_mid_jnt, limb_fk_tip_jnt]
        limb_fk_base_jnt.orient('xyz', 'xup', True, True)

        # --------------------------------------------------------------------------------------------------------------
        # create fk controls
        # --------------------------------------------------------------------------------------------------------------
        limb_fk_ctrls = []
        for limb_fk_jnt in limb_fk_jnts:
            limb_fk_ctrl = Control(limb_fk_jnt.name.replace('_jnt', '_ctrl'),
                                   worldMatrix=limb_fk_jnt.worldMatrix,
                                   rotateOrder='zxy')
            limb_fk_ctrl.rotate = (0.0, 0.0, 0.0)
            limb_fk_ctrl.create_null_grps()
            limb_fk_ctrls.append(limb_fk_ctrl)

            if limb_fk_ctrls.index(limb_fk_ctrl) > 0:
                limb_fk_ctrl.null_grp.parent = limb_fk_ctrls[limb_fk_ctrls.index(limb_fk_ctrl) - 1]

            cmds.pointConstraint(limb_fk_ctrl, limb_fk_jnt, maintainOffset=True)
            cmds.orientConstraint(limb_fk_ctrl, limb_fk_jnt, maintainOffset=True)

        # --------------------------------------------------------------------------------------------------------------
        # add fk stretch
        # --------------------------------------------------------------------------------------------------------------
        for limb_fk_ctrl in limb_fk_ctrls:
            cmds.addAttr(limb_fk_ctrl, shortName='stretch', attributeType='float', readable=True, storable=True,
                         keyable=True, min=0, defaultValue=1)

            limb_fk_ctrl_stretch_factor_div = XNode('{}_stretch_factor_div'.format(limb_fk_ctrl),
                                                    node_type='multiplyDivide',
                                                    operation=2)

            limb_fk_ctrl.stretch >> limb_fk_ctrl_stretch_factor_div.input1X

        # --------------------------------------------------------------------------------------------------------------
        # setup ik/fk blending
        # --------------------------------------------------------------------------------------------------------------
        result_jnts = [limb_result_base_jnt, limb_result_mid_jnt, limb_result_tip_jnt]
        fk_jnts = [limb_fk_base_jnt, limb_fk_mid_jnt, limb_fk_tip_jnt]
        ik_jnts = [limb_ik_base_jnt, limb_ik_mid_jnt, limb_ik_tip_jnt]
        blend_nodes = []

        for src_jnt, ik_jnt, fk_jnt in zip(result_jnts, ik_jnts, fk_jnts):
            blend_nodes.append(self.setup_limb_chain_blend(src_jnt, ik_jnt, fk_jnt, 'translate'))
            blend_nodes.append(self.setup_limb_chain_blend(src_jnt, ik_jnt, fk_jnt, 'rotate'))

        # --------------------------------------------------------------------------------------------------------------
        # setup ik/fk blending control
        # --------------------------------------------------------------------------------------------------------------
        limb_fk_ik_blend_ctrl = Control('{}_fk_ik_blend_ctrl'.format(self.name),
                                        control_shape='gear')

        # print limb_fk_ik_blend_ctrl.worldMatrix
        # print limb_result_tip_jnt.worldMatrix

        cmds.addAttr(limb_fk_ik_blend_ctrl,
                     shortName='fk_ik_blend',
                     longName='fk_ik_blend',
                     keyable=True, readable=True, storable=True, min=0, max=1, defaultValue=0)

        for blend_node in blend_nodes:
            limb_fk_ik_blend_ctrl.fk_ik_blend >> blend_node.blender

        limb_fk_ik_blend_ctrl.worldMatrix = limb_result_tip_jnt.worldMatrix
        limb_fk_ik_blend_ctrl.rotate = (0.0, 0.0, 0.0)
        limb_fk_ik_blend_ctrl.create_null_grps()
        limb_fk_ik_blend_ctrl.tx.lock
        limb_fk_ik_blend_ctrl.tx.hide


        # --------------------------------------------------------------------------------------------------------------
        # group nodes
        # --------------------------------------------------------------------------------------------------------------
        limb_result_base_jnt.parent = limb_do_not_touch_grp
        limb_ik_base_jnt.parent = limb_do_not_touch_grp
        limb_fk_base_jnt.parent = limb_do_not_touch_grp
        limb_ik_handle.parent = limb_do_not_touch_grp
        limb_ik_root_null.parent = limb_do_not_touch_grp

        limb_fk_ctrls[0].null_grp.parent = limb_ctrl_grp
        limb_ik_ctrl.null_grp.parent = limb_ctrl_grp
        limb_fk_ik_blend_ctrl.null_grp.parent = limb_ctrl_grp
        limb_ik_pv_ctrl.null_grp.parent = limb_ctrl_grp

        limb_ik_ctrl_point_constraint = cmds.pointConstraint(limb_ik_ctrl, limb_ik_handle)
        limb_ik_ctrl_orient_constraint = cmds.orientConstraint(limb_ik_ctrl, limb_ik_handle)

        # --------------------------------------------------------------------------------------------------------------
        # add hook nodes to __dict__
        # --------------------------------------------------------------------------------------------------------------
        self.limb_ik_handle = limb_ik_handle
        self.limb_ik_base_jnt = limb_ik_base_jnt
        self.limb_ik_mid_jnt = limb_ik_mid_jnt
        self.limb_ik_tip_jnt = limb_ik_tip_jnt
        self.limb_do_not_touch_grp = limb_do_not_touch_grp
        self.limb_ik_ctrl = limb_ik_ctrl
        self.limb_ik_ctrl_stretch_ctrl_dist = limb_ik_ctrl_stretch_ctrl_dist
        self.limb_ik_pv_ctrl_pin_tip_dist = limb_ik_pv_ctrl_pin_tip_dist
        self.limb_ik_ctrl_point_constraint = limb_ik_ctrl_point_constraint
        self.limb_ik_ctrl_orient_constraint = limb_ik_ctrl_orient_constraint

    def hook(self):
        pass

    @staticmethod
    def setup_limb_chain_blend(src_jnt, ik_jnt, fk_jnt, attr):
        blend_node = XNode('{}_{}_blend'.format(src_jnt, attr), node_type='blendColors')
        ik_jnt.connect(attr, blend_node, 'color1')
        fk_jnt.connect(attr, blend_node, 'color2')
        blend_node.connect('output', src_jnt, attr)
        return blend_node

    @staticmethod
    def calculate_pv_ctrl_pos(root_jnt, mid_jnt, tip_jnt, vector_scale=1.0):
        root_vec = xrig.util.maya_api.create_mvector(root_jnt)
        mid_vec = xrig.util.maya_api.create_mvector(mid_jnt)
        tip_vec = xrig.util.maya_api.create_mvector(tip_jnt)

        root_to_tip_vec = tip_vec - root_vec
        root_to_mid_vec = mid_vec - root_vec

        dot_prod = root_to_mid_vec * root_to_tip_vec
        projected_length = float(dot_prod) / float(root_to_tip_vec.length())
        norm_root_to_tip_vec = root_to_tip_vec.normal()
        projected_vec = norm_root_to_tip_vec * projected_length
        arrow_vec = root_to_mid_vec - projected_vec
        arrow_vec *= vector_scale

        result_vec = arrow_vec + mid_vec
        return result_vec.x, result_vec.y, result_vec.z
