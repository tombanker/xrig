from xrig.base.limb import Limb
from xrig.base.control import Control
from xrig.base.joint import Joint
from xrig.base.xnode import XNode
import maya.cmds as cmds


class FKLimb(Limb):
    def __init__(self, name, base, control_shape='circle', prefix='l_'):
        super(FKLimb, self).__init__(name='fklimb')
        self.name = name
        self.control_shape = control_shape
        self.prefix = prefix
        self.name = '{}{}'.format(self.prefix, self.name)

        self.limb_base = XNode(name=base)

    def create(self):
        self.limb_jnt = Joint('{}_fk_jnt'.format(self.name), worldMatrix=self.limb_base.worldMatrix)
        self.limb_ctrl = Control('{}_fk_ctrl'.format(self.name), control_shape=self.control_shape, worldMatrix=self.limb_base.worldMatrix)
        cmds.pointConstraint(self.limb_ctrl, self.limb_jnt)
        cmds.orientConstraint(self.limb_ctrl, self.limb_jnt)

    def hook(self):
        pass
