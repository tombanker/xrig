"""
Data object for representing a maya joint node
"""
import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
import maya.OpenMayaAnim as OpenMayaAnim
import xrig.base.xnode
import xrig.util.maya_api


# --------------------------------------------------------------------------------------------------------------
# utility methods
# --------------------------------------------------------------------------------------------------------------
def insert_joints(start_joint, end_joint, num_mid_joints, prefix=''):
    # get the start and end vectors for the template joints
    start_joint_vector = xrig.util.maya_api.create_mvector(start_joint)
    end_joint_vector = xrig.util.maya_api.create_mvector(end_joint)
    total_chain_vector = end_joint_vector - start_joint_vector
    total_chain_length = total_chain_vector.length()

    # insert i number of joints evenly spaced between the template joints
    mid_joints = []
    for i in range(1, num_mid_joints + 1):
        segment_length = float(total_chain_length) / (num_mid_joints + 1)
        iter_segment_length = float(segment_length) * i
        iter_segment_length_mult = iter_segment_length / total_chain_length

        segment_vector = ((end_joint_vector - start_joint_vector) * iter_segment_length_mult) + start_joint_vector
        segment_joint = Joint(name='{0}_{1}_jnt'.format(prefix, str(i).rjust(2, '0')))
        segment_joint.t = (segment_vector.x, segment_vector.y, segment_vector.z)
        mid_joints.append(segment_joint)

    # # parent mid joints into a start and end joint chain
    if mid_joints:
        mid_joints[0].parent = start_joint
        for i in mid_joints[1:]:
            i.parent = mid_joints[mid_joints.index(i) - 1]
        end_joint.parent = mid_joints[-1]

    return mid_joints


# --------------------------------------------------------------------------------------------------------------
# create solve joints
# --------------------------------------------------------------------------------------------------------------
class Joint(xrig.base.xnode.XNode):
    node_type = 'joint'
    side_label_dict = {0: 'center', 1: 'left', 2: 'right', 3: 'none'}
    type_label_dict = {0: 'none', 1: 'root', 2: 'hip', 3: 'knee', 4: 'foot', 5: 'toe', 6: 'spine', 7: 'neck', 8: 'head',
                       9: 'collar', 10: 'shoulder', 11: 'elbow', 12: 'hand', 13: 'finger', 14: 'thumb', 15: 'propa',
                       16: 'propb', 17: 'propc', 18: 'other', 19: 'index finger', 20: 'middle finger',
                       21: 'ring finger', 22: 'pinky finger', 23: 'extra finger', 24: 'big toe', 25: 'index toe',
                       26: 'middle toe', 27: 'ring toe', 28: 'pinky toe', 29: 'foot thumb'}

    def __init__(self, *args, **kwargs):
        super(Joint, self).__init__(*args, **kwargs)

    def orient(self, orient_joint, secondary_axis_orient, zero_scale_orient, children):
        cmds.joint(self.name,
                   orientJoint=orient_joint,
                   secondaryAxisOrient=secondary_axis_orient,
                   zeroScaleOrient=zero_scale_orient,
                   children=children,
                   edit=True)

        self.chain[-1].jointOrient = (0.0, 0.0, 0.0)

    @property
    def chain(self):
        joint_chain_list = []
        joint_chain = cmds.listRelatives(self.name, allDescendents=True, type='joint')[::-1]
        joint_chain.insert(0, self.name)

        for jnt in joint_chain:
            joint_chain_list.append(Joint(jnt))

        return joint_chain_list

    def freeze(self):
        cmds.makeIdentity(self.name, translate=True, rotate=True, scale=True, apply=True)
