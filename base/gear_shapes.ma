//Maya ASCII 2018 scene
//Name: gear_shapes.ma
//Last modified: Mon, Jul 23, 2018 11:27:59 PM
//Codeset: 1252
requires maya "2018";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "6D1ABF7E-4A10-CE93-8076-61AB3AC9DC0F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.312751902102832 10.429349565822724 -7.146871253880601 ;
	setAttr ".r" -type "double3" -57.305266384489983 -547.40000000004898 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "62F16453-41DA-A66B-643F-038AE19251D7";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 13.024483198171458;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.69579823886780179 1.6676161046697733e-17 -0.27234237754605367 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "53EC91E8-42A3-FDD4-2538-D7A232717F43";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.89323061156606398 1000.1000000357151 1.122628432044112 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2FE7FF93-4649-9464-02CA-C297B1CABB6B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1000000357151;
	setAttr ".ow" 24.993175119108162;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 0.0084520578384399414 0 -1.1920928955078125e-07 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "A41DEB80-4817-E628-C615-5CA0D419EADE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "203F9219-4ED3-F96C-3197-7D8FB96C120D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "229A91FA-4B35-063B-F264-52A08459A16E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "7832E8EF-4685-C7BA-E4CF-F9B966975A34";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "gear";
	rename -uid "E2E8556F-467D-68FC-9CA8-899D1DC22390";
	setAttr ".rp" -type "double3" 0 0 1.1920928955078125e-07 ;
	setAttr ".sp" -type "double3" 0 0 1.1920928955078125e-07 ;
createNode nurbsCurve -n "gearShape" -p "gear";
	rename -uid "C0242612-40E9-08B7-EBBB-A2A3FBA23075";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 32 0 no 3
		33 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32
		33
		-0.14631816744804382 0 2.7355890274047852
		-0.91168504953384399 0 2.5833477973937988
		-0.76536720991134644 0 1.8477588891983032
		-1.4142138957977295 0 1.4142131805419922
		-2.0378162860870361 0 1.8308906555175781
		-2.4713616371154785 0 1.1820437908172607
		-1.8477592468261719 0 0.76536625623703003
		-2 0 -7.152557373046875e-07
		-2.7355890274047852 0 -0.14631874859333038
		-2.5833477973937988 0 -0.91168564558029175
		-1.8477587699890137 0 -0.76536756753921509
		-1.4142129421234131 0 -1.4142141342163086
		-1.830890417098999 0 -2.0378165245056152
		-1.1820433139801025 0 -2.4713618755340576
		-0.76536595821380615 0 -1.8477593660354614
		1.0132789611816406e-06 0 -1.9999998807907104
		0.14631916582584381 0 -2.7355887889862061
		0.9116860032081604 0 -2.5833475589752197
		0.76536780595779419 0 -1.8477585315704346
		1.4142142534255981 0 -1.414212703704834
		2.0378165245056152 0 -1.8308901786804199
		2.4713618755340576 0 -1.182043194770813
		1.847759485244751 0 -0.76536577939987183
		2 0 0
		2.7355890274047852 0 0.14631778001785278
		2.5833480358123779 0 0.91168463230133057
		1.8477590084075928 0 0.76536691188812256
		1.4142134189605713 0 1.4142136573791504
		1.8308908939361572 0 2.0378158092498779
		1.1820442676544189 0 2.4713613986968994
		0.76536661386489868 0 1.8477591276168823
		-2.9802322387695313e-07 0 2
		-0.14631816744804382 0 2.7355890274047852
		;
createNode nurbsCurve -n "gearShape1" -p "gear";
	rename -uid "E1176E6B-4882-800E-9437-C4A3E356E530";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.97951453111403064 5.9977966762355913e-17 -0.97951453111403075
		8.4821654038886399e-17 8.4821654038886399e-17 -1.3852427344429845
		-0.97951453111403064 5.99779667623559e-17 -0.97951453111403053
		-1.3852427344429852 4.3971695237575336e-33 -7.1811227969060383e-17
		-0.97951453111403064 -5.99779667623559e-17 0.97951453111403064
		-1.3876071212004032e-16 -8.4821654038886461e-17 1.3852427344429854
		0.97951453111403064 -5.99779667623559e-17 0.97951453111403053
		1.3852427344429852 -1.1567099012637624e-32 1.8890506259749487e-16
		0.97951453111403064 5.9977966762355913e-17 -0.97951453111403075
		8.4821654038886399e-17 8.4821654038886399e-17 -1.3852427344429845
		-0.97951453111403064 5.99779667623559e-17 -0.97951453111403053
		;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "2209E687-44CF-6BA2-1AC6-809CF85F4F12";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "66C0C2E5-4C46-7FF6-0107-B78B3FDA11BC";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "862BC8C6-48D6-57B5-C49E-009FDC25CD75";
createNode displayLayerManager -n "layerManager";
	rename -uid "E256AA1B-43BE-A6EE-B944-31B47C48860F";
createNode displayLayer -n "defaultLayer";
	rename -uid "DE5D9125-47F0-4413-5412-D191372FD7DF";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "68ED34F2-4164-7356-2656-0F80F8ADF05C";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "EF084B36-421D-CFB2-329B-3689E14BA7DE";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "6C3D5E8C-48C7-4645-76FC-279A6274C143";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -av ".aoam";
	setAttr -k on ".mbsof";
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :ikSystem;
	setAttr -av ".gsn";
	setAttr -s 4 ".sol";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of gear_shapes.ma
