"""
Module Docstring
"""

__author__ = 'mikura'
__version__ = 1138.0

import maya.cmds as cmds
import xrig.util.maya_api
import xrig.base.xnode


class MetaNode(xrig.base.xnode.XNode):
    def __init__(self, name, *args, **kwargs):
        self.node_type = 'network'
        super(MetaNode, self).__init__(name=name)

        cmds.addAttr(self.name, longName='msg_single_test', attributeType='message')
