"""
Module Docstring
"""

__author__ = 'jleach'
__version__ = "0.0.25"

import os
import maya.cmds as cmds
import xnode


class Control(xnode.XNode):
    def __init__(self, name, control_shape='circle', *args, **kwargs):
        self.control_shape = control_shape
        self.null_grp = None
        self.offset_grp = None
        self.anim_grp = None
        self.const_grp = None
        super(Control, self).__init__(name, control_shape, *args, **kwargs)

    # @xnode.overrides(xnode.XNode)
    def create(self):
        cmds.select(clear=True)
        control_shapes_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'control_shapes.ma'))

        try:
            cmds.file(control_shapes_path, reference=True, namespace='control_shapes')
            control = cmds.duplicate('control_shapes:{}'.format(self.control_shape))
        except ValueError:
            cmds.warning('Shape type: %s not found.' % self.control_shape)
            cmds.file(control_shapes_path, removeReference=True)
            return None

        cmds.file(control_shapes_path, removeReference=True)
        control = cmds.rename(control, self.name)
        cmds.delete(self.name, constructionHistory=True)
        cmds.setAttr('%s.rotateOrder' % self.name, channelBox=True, keyable=False)
        self.name = control
        self.add_metadata_attrs(metadata_type='control', metadata_version=__version__, metadata_author=__author__)

        # set color
        # self.overrideEnabled = 1
        #
        # if self.name.startswith('l_'):
        #     self.overrideColor = 6
        # elif self.name.startswith('r_'):
        #     self.overrideColor = 13
        # else:
        #     self.overrideColor = 17

    def create_null_grps(self):
        cmds.select(clear=True)
        self.null_grp = xnode.XNode(name='{}_null_grp'.format(self.name))
        # self.offset_grp = xnode.XNode(name='offset_{}_grp'.format(self.name), parent=self.null_grp)
        self.anim_grp = xnode.XNode(name='{}_anim_grp'.format(self.name), parent=self.null_grp)
        self.const_grp = xnode.XNode(name='{}_const_grp'.format(self.name), parent=self.anim_grp)

        self.parent = self.const_grp
        self.null_grp.matrix = self.matrix
        self.matrix = [1.0, 0.0, 0.0, 0.0,
                       0.0, 1.0, 0.0, 0.0,
                       0.0, 0.0, 1.0, 0.0,
                       0.0, 0.0, 0.0, 1.0]

    @staticmethod
    def add_space_switching(dst_node, dst_attr, constraint_type, targets=None):
        if constraint_type == 'point':
            space_const = cmds.pointConstraint(targets, dst_node, maintainOffset=True)[0]
            const_node_targets = cmds.pointConstraint(space_const, weightAliasList=True, query=True)
        elif constraint_type == 'orient':
            space_const = cmds.orientConstraint(targets, dst_node, maintainOffset=True)[0]
            const_node_targets = cmds.orientConstraint(space_const, weightAliasList=True, query=True)
        elif constraint_type == 'parent':
            space_const = cmds.parentConstraint(targets, dst_node, maintainOffset=True)[0]
            const_node_targets = cmds.parentConstraint(space_const, weightAliasList=True, query=True)
        else:
            return False

        for index, target in enumerate(zip(targets, const_node_targets)):
            cond_node = xnode.XNode('{}_{}_cond'.format(target[0], constraint_type), node_type='condition')
            cmds.connectAttr('{}'.format(dst_attr), '{}.firstTerm'.format(cond_node), force=True)
            cmds.setAttr('{}.secondTerm'.format(cond_node), index)
            cmds.setAttr('{}.colorIfTrueR'.format(cond_node), 1)
            cmds.setAttr('{}.colorIfFalseR'.format(cond_node), 0)
            cmds.connectAttr('{}.outColorR'.format(cond_node), '{}.{}'.format(space_const, target[1]), force=True)

        return space_const
