"""
Module Docstring
"""

__author__ = 'tbanker'
__version__ = "1.0.0"

import maya.cmds as cmds
import xrig.util.maya_api
import json

"""
https://stackoverflow.com/questions/31446603/how-to-make-type-cast-for-python-custom-class

class MyClass:

    @staticmethod
    def from_json(document):
        data = json.loads(document)
        instance = MyClass(data['value'])
        return instance

    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        return isinstance(self, MyClass) and self.value == other.value

    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, self.value)

    def to_json(self):
        data = {
            'value': self.value
        }
        return json.dumps(data)
"""


# ------------------------------------------------------------------------------------------------------------------
# Utility Methods
# ------------------------------------------------------------------------------------------------------------------
def overrides(parent_class):
    def overrider(method):
        assert (method._name__ in dir(parent_class))
        return method

    return overrider


class XAttr(object):
    # ------------------------------------------------------------------------------------------------------------------
    # Configurations & Storage
    # ------------------------------------------------------------------------------------------------------------------
    node_type = 'transform'
    rotate_order_dict = {0: 'xyz', 1: 'yzx', 2: 'zxy', 3: 'xzy', 4: 'yxz', 5: 'zyx'}
    display_type_dict = {0: 'normal', 1: 'template', 2: 'reference'}
    color_dict = {0: 'default', 1: 'black', 2: 'dark grey', 3: 'light grey', 4: 'burgundy', 5: 'dark blue', 6: 'blue',
                  7: 'dark green', 8: 'dark purple', 9: 'magenta', 10: 'beige', 11: 'dark beige', 12: 'brown',
                  13: 'red', 14: 'green', 15: 'sky blue', 16: 'white', 17: 'yellow', 18: 'cyan', 19: 'aqua', 20: 'pink',
                  21: 'skin', 22: 'pale yellow', 23: 'forest green', 24: 'copper', 25: 'gold', 26: 'sushi',
                  27: 'sea green', 28: 'keppel', 29: 'azure', 30: 'royal purple', 31: 'purple'}

    # ------------------------------------------------------------------------------------------------------------------
    # Initialization
    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, name):
        self.name = name
        self.node, self.attr = name.split('.')

    # ------------------------------------------------------------------------------------------------------------------
    # Overridden Methods
    # ------------------------------------------------------------------------------------------------------------------
    def __getattribute__(self, item):
        if object.__getattribute__(self, 'name'):
            _node = object.__getattribute__(self, 'node')

            if cmds.objExists(_node):
                _attr = object.__getattribute__(self, 'attr')

                if cmds.attributeQuery(_attr, node=_node, exists=True):
                    _value = cmds.getAttr('{}.{}'.format(_node, _attr))
                    _dict = object.__getattribute__(self, '__dict__')

                    if isinstance(_value, list):
                        if isinstance(_value[0], tuple):
                            _dict.update({'value': _value[0]})
                            # return _dict
                            # return _value[0]
                        else:
                            _dict.update({'value': _value})
                    else:
                        _dict.update({'value': _value})
                    # return _dict
                    # return _value
        return object.__getattribute__(self, item)

    # def __getattribute__(self, item, *args, **kwargs):
    #     # print '__getattribute__', item, args, kwargs
    #     if object.__getattribute__(self, 'name'):
    #         _name = object.__getattribute__(self, 'name')
    #
    #         if cmds.objExists(_name):
    #             if item == 'parent':
    #                 if 'parent' not in self.__dict__.keys():
    #                     return None
    #                 return cmds.listRelatives(_name, parent=True)
    #
    # if cmds.attributeQuery(item, node=_name, exists=True):
    #     result = cmds.getAttr('{}.{}'.format(_name, item))
    #     if isinstance(result, list):
    #         if isinstance(result[0], tuple):
    #             return result[0]
    #     return result

    def __setattr__(self, key, value):
        if hasattr(self, 'node'):
            _node = object.__getattribute__(self, 'node')

            if isinstance(value, XAttr):
                value = value.value

            try:
                if isinstance(value, list) or isinstance(value, tuple):
                    if len(value) == 3:
                        for index, axis in enumerate(['X', 'Y', 'Z']):
                            rounded_value = round(value[index], 5)

                            try:
                                # jointOrientX works, jointOrientx doesn't
                                cmds.setAttr('{}.{}{}'.format(_node, key, axis), rounded_value)
                            except RuntimeError as e:
                                # tx works, tX doesn't
                                cmds.setAttr('{}.{}{}'.format(_node, key, axis.lower()), rounded_value)

                    if len(value) == 16:
                        if key == 'worldMatrix' or key == 'wm':
                            cmds.xform(_node, matrix=value, worldSpace=True)
                        elif key == 'matrix' or key == 'm':
                            cmds.xform(_node, matrix=value, worldSpace=False)

                elif isinstance(value, str):
                    if key == 'rotateOrder' and isinstance(value, str):
                        value = self.rotate_order_dict.keys()[self.rotate_order_dict.values().index(value)]
                        cmds.setAttr('{}.{}'.format(_node, key), value)
                    else:
                        cmds.setAttr('{}.{}'.format(_node, key), value, type='string')

                else:
                    if key == 'rotateOrder' and isinstance(value, str):
                        value = self.rotate_order_dict.keys()[self.rotate_order_dict.values().index(value)]
                    cmds.setAttr('{}.{}'.format(_node, key), value)

            except RuntimeError as e:
                print(e)
                print('ERROR: Wrong attribute data type: {}.{}: {}'.format(_node, key, value))
                return
        #
        # elif key not in self.__dict__.keys():
        #     print('ERROR: Could not set attribute: {}.{}'.format(self, key))

        object.__setattr__(self, key, value)

    # def __str__(self):
    # serialize and unserialize data to get correct attribute "type" instead of type of class instance
    # out_data = json.dumps(self.__dict__)
    # in_data = json.loads(out_data)
    # return str(in_data.get('value'))
    # return str(self.__dict__.get('value'))
    # print self.__dict__
    # return str(self.__dict__.get('value'))
    # return str(self.name)

    def __str__(self):
        return str(self.__dict__.get('value'))

    def __rshift__(self, other):
        if isinstance(self, XAttr) and isinstance(other, XAttr):
            cmds.connectAttr(self.name, other.name, force=True)

    def __floordiv__(self, other):
        if isinstance(self, XAttr) and isinstance(other, XAttr):
            cmds.disconnectAttr(self.name, other.name, force=True)

    # ------------------------------------------------------------------------------------------------------------------
    # Class Properties
    # ------------------------------------------------------------------------------------------------------------------
    @property
    def lock(self):
        cmds.setAttr(self.name, lock=True)
        return True

    @property
    def unlock(self):
        cmds.setAttr(self.name, lock=False)
        return True

    @property
    def hide(self):
        cmds.setAttr(self.name, keyable=False, channelBox=False)
        return True

    @property
    def unhide(self):
        cmds.setAttr(self.name, channelBox=True)
        self.keyable
        return True

    @property
    def keyable(self):
        cmds.setAttr(self.name, keyable=True)
        return True

    @property
    def non_keyable(self):
        cmds.setAttr(self.name, keyable=False, channelBox=True)
        return True

    @property
    def lock_and_hide(self):
        self.unhide
        self.unlock
        self.hide
        self.lock
        return True

    @property
    def unlock_and_unhide(self):
        self.unhide
        self.unlock
        return True


class XNode(object):
    # ------------------------------------------------------------------------------------------------------------------
    # Configurations & Storage
    # ------------------------------------------------------------------------------------------------------------------
    node_type = 'transform'
    rotate_order_dict = {0: 'xyz', 1: 'yzx', 2: 'zxy', 3: 'xzy', 4: 'yxz', 5: 'zyx'}
    display_type_dict = {0: 'normal', 1: 'template', 2: 'reference'}
    color_dict = {0: 'default', 1: 'black', 2: 'dark grey', 3: 'light grey', 4: 'burgundy', 5: 'dark blue', 6: 'blue',
                  7: 'dark green', 8: 'dark purple', 9: 'magenta', 10: 'beige', 11: 'dark beige', 12: 'brown',
                  13: 'red', 14: 'green', 15: 'sky blue', 16: 'white', 17: 'yellow', 18: 'cyan', 19: 'aqua', 20: 'pink',
                  21: 'skin', 22: 'pale yellow', 23: 'forest green', 24: 'copper', 25: 'gold', 26: 'sushi',
                  27: 'sea green', 28: 'keppel', 29: 'azure', 30: 'royal purple', 31: 'purple'}

    # ------------------------------------------------------------------------------------------------------------------
    # Initialization
    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, name, *args, **kwargs):
        # print 'xnode __init__', name, args, kwargs
        self.name = name
        self.__dict__.update(kwargs)
        self.create()

        for key, value in kwargs.items():
            self.__setattr__(key, value)

    # ------------------------------------------------------------------------------------------------------------------
    # Overridden Methods
    # ------------------------------------------------------------------------------------------------------------------
    def __getattribute__(self, item, *args, **kwargs):
        if object.__getattribute__(self, 'name'):
            _name = object.__getattribute__(self, 'name')

            if cmds.objExists(_name):
                if item == 'parent':
                    if 'parent' not in self.__dict__.keys():
                        return None
                    return cmds.listRelatives(_name, parent=True)

                if cmds.attributeQuery(item, node=_name, exists=True):
                    # -
                    # -
                    # - Testing
                    # -
                    # -
                    return XAttr(name='{}.{}'.format(_name, item))
                    # -
                    # -
                    # - Testing
                    # -
                    # -

                    # result = cmds.getAttr('{}.{}'.format(_name, item))
                    # if isinstance(result, list):
                    #     if isinstance(result[0], tuple):
                    #         return result[0]
                    # return result

        return object.__getattribute__(self, item)

    def __setattr__(self, key, value):
        # print '__setattr__', key, value
        if hasattr(self, 'name'):
            _name = object.__getattribute__(self, 'name')

            if cmds.objExists(_name):
                if key == 'parent':
                    parent_node = cmds.listRelatives(_name, parent=True)

                    if not value:
                        if parent_node:
                            cmds.parent(_name, world=True)
                    else:
                        child_parent_node = cmds.listRelatives(str(value), parent=True)

                        if parent_node:
                            if parent_node[0] == str(value):
                                print 'ERROR: Object {} already parented under {}'.format(_name, str(value))
                                return

                        if child_parent_node:
                            if child_parent_node[0] == _name:
                                print 'ERROR: Cannot parent object {} to its child: {}'.format(_name, str(value))
                                return

                        cmds.parent(_name, str(value))

                elif key == 'name':
                    cmds.rename(_name, value)

                elif cmds.attributeQuery(key, node=_name, exists=True):
                    XAttr(name='{}.{}'.format(self, key)).__setattr__(key, value)

        object.__setattr__(self, key, value)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __rshift__(self, other, *args, **kwargs):
        print self, other, args, kwargs

        if isinstance(self, (int, long, float)) and isinstance(other, (int, long, float)):
            return '0'
        elif isinstance(self, XNode):
            return self
        else:
            return '1'

    # ------------------------------------------------------------------------------------------------------------------
    # Class Methods
    # ------------------------------------------------------------------------------------------------------------------
    def create(self):
        if not cmds.objExists(self.name):
            if self.node_type in cmds.allNodeTypes():
                cmds.createNode(self.node_type, name=self.name)
                self.add_metadata_attrs()
            else:
                self.node_type = 'transform'
                cmds.createNode(self.node_type, name=self.name)
                self.add_metadata_attrs()
        return self.name

    def connect(self, in_attr, node, out_attr, force=True):
        return cmds.connectAttr('{}.{}'.format(self, in_attr), '{}.{}'.format(node, out_attr), force=force)

    def add_metadata_attrs(self, **kwargs):
        cmds.addAttr(self.name, shortName='metadata_version', dataType='string')
        cmds.addAttr(self.name, shortName='metadata_author', dataType='string')
        cmds.addAttr(self.name, shortName='metadata_class', dataType='string')
        cmds.addAttr(self.name, shortName='metadata_name', dataType='string')
        cmds.addAttr(self.name, shortName='metadata_type', dataType='string')
        cmds.addAttr(self.name, shortName='metadata_limb', dataType='string')

        # default argument setting
        try:
            cmds.setAttr('{}.metadata_author'.format(self.name), __author__, type='string')
            cmds.setAttr('{}.metadata_version'.format(self.name), __version__, type='string')
        except:
            cmds.setAttr('{}.metadata_author'.format(self.name), '', type='string')
            cmds.setAttr('{}.metadata_version'.format(self.name), '', type='string')

        # print self.__module__
        # print dir(self.__module__)
        # print self.__class__
        # print dir(self.__class__)

        cmds.setAttr('{}.metadata_class'.format(self.name), self.__class__, type='string')
        cmds.setAttr('{}.metadata_name'.format(self.name), self.name, type='string')
        cmds.setAttr('{}.metadata_type'.format(self.name), self.node_type, type='string')
        cmds.setAttr('{}.metadata_limb'.format(self.name), self.name.split('_')[0], type='string')

        # set the variable name if it's given in the method call
        for arg in kwargs:
            cmds.setAttr('{}.{}'.format(self.name, arg), kwargs.get(arg), type='string')

        # case handling for limbs that start with '{side}_'
        if self.name.startswith('l_') or self.name.startswith('r_'):
            cmds.setAttr('{}.metadata_limb'.format(self.name), '_'.join(self.name.split('_')[0:2]), type='string')

    # def lock_attr(self, attr, channel_box=False, keyable=False):
    #     mplug = xrig.util.maya_api.get_mplug(self, attr)
    #
    #     if mplug.isCompound():
    #         print mplug.numChildren()
    #
    #     mplug.setKeyable(keyable)
    #     mplug.setLocked(True)
    #     mplug.setChannelBox(channel_box)

    # ------------------------------------------------------------------------------------------------------------------
    # Class Properties
    # ------------------------------------------------------------------------------------------------------------------
    @property
    def child(self):
        return cmds.listRelatives(self.name, children=True)

    @property
    def children(self):
        return cmds.listRelatives(self.name, allDescendents=True)

    @property
    def shapes(self):
        # return asset_tools.maya.utils.maya_api.get_shape_nodes(self.name)
        return xrig.util.maya_api.get_shapes(self.name)


"""
import sys
path = 'e:/'
if path not in sys.path:
    sys.path.append(path)

import xrig.base.xnode
reload(xrig.base.xnode)
xrig.base.xnode.main()

root = xrig.base.xnode.XNode(name='root', node_type='joint', radius=5)
hip = xrig.base.xnode.XNode(name='hip', node_type='joint', tx=50, radius=5, parent=root)
leg = xrig.base.xnode.XNode(name='leg', node_type='joint', ty=50, radius=5)
"""

"""
    def __setattr__(self, key, value):
        # print '__setattr__', key, value
        if hasattr(self, 'name'):
            _name = object.__getattribute__(self, 'name')

            if cmds.objExists(_name):
                if key == 'parent':
                    parent_node = cmds.listRelatives(_name, parent=True)

                    if not value:
                        if parent_node:
                            cmds.parent(_name, world=True)
                    else:
                        child_parent_node = cmds.listRelatives(str(value), parent=True)

                        if parent_node:
                            if parent_node[0] == str(value):
                                print 'ERROR: Object {} already parented under {}'.format(_name, str(value))
                                return

                        if child_parent_node:
                            if child_parent_node[0] == _name:
                                print 'ERROR: Cannot parent object {} to its child: {}'.format(_name, str(value))
                                return

                        cmds.parent(_name, str(value))

                elif key == 'name':
                    cmds.rename(_name, value)

                elif cmds.attributeQuery(key, node=_name, exists=True):
                    try:
                        if isinstance(value, list) or isinstance(value, tuple):
                            if len(value) == 3:
                                for index, axis in enumerate(['X', 'Y', 'Z']):
                                    rounded_value = round(value[index], 5)

                                    try:
                                        # jointOrientX works, jointOrientx doesn't
                                        cmds.setAttr('{}.{}{}'.format(_name, key, axis), rounded_value)
                                    except RuntimeError as e:
                                        print (e)
                                        # tx works, tX doesn't
                                        cmds.setAttr('{}.{}{}'.format(_name, key, axis.lower()), rounded_value)

                            if len(value) == 16:
                                if key == 'worldMatrix' or key == 'wm':
                                    cmds.xform(_name, matrix=value, worldSpace=True)
                                elif key == 'matrix' or key == 'm':
                                    cmds.xform(_name, matrix=value, worldSpace=False)

                        else:
                            if key == 'rotateOrder' and isinstance(value, str):
                                value = self.rotate_order_dict.keys()[self.rotate_order_dict.values().index(value)]
                            cmds.setAttr('{}.{}'.format(_name, key), value)

                    except RuntimeError as e:
                        print(e)
                        print('ERROR: Wrong attribute data type: {}.{}: {}'.format(_name, key, value))
                        return

                elif key not in self.__dict__.keys():
                    print('ERROR: Could not set attribute: {}.{}'.format(self.name, key))
                    
                object.__setattr__(self, key, value)
"""
