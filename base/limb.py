import xrig.base.joint
import maya.cmds as cmds


class Limb(object):
    public_attrs = []

    def __init__(self, name, prefix=''):
        self.name = name
        self.prefix = prefix
        self.ctrls = []
        self.result_jnts = []
        self.env_jnts = []
        self.pivot_nodes = []

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def create(self):
        raise NotImplementedError

    def hook(self, *args, **kwargs):
        raise NotImplementedError

    def mirror(self, mirror_plane='yz', mirror_prefix='r_'):
        self.name = self.name.replace(self.prefix, mirror_prefix)
        self.prefix = mirror_prefix

        # print 'self.name:', self.name
        # print 'self.prefix:', self.prefix
        # print 'self.ctrls:', self.ctrls
        # print 'self.result_jnts:', self.result_jnts
        # print 'self.env_jnts:', self.env_jnts
        if self.pivot_nodes:
            for pivot_node in self.pivot_nodes:
                import ast
                from maya.api.OpenMaya import MMatrix

                pivot_node_mmatrix = MMatrix(ast.literal_eval(str(pivot_node.worldMatrix)))
                mirrored_mmatrix = MMatrix([-1, 0, 0, 0,
                                            0, 1, 0, 0,
                                            0, 0, 1, 0,
                                            0, 0, 0, 1])

                result_matrix = pivot_node_mmatrix * mirrored_mmatrix
                pivot_node.worldMatrix = list(result_matrix)
                cmds.makeIdentity(pivot_node, rotate=True, scale=True)

        self.create()

    def get_ctrls(self):
        return self.ctrls

    def get_joints(self):
        return self.result_jnts

    def create_env_jnts(self):
        for result_jnt in self.result_jnts:
            result_jnt = xrig.base.joint.Joint(name=result_jnt)

            env_jnt_name = str(result_jnt.name).replace('_jnt', '_env_jnt')
            env_jnt = xrig.base.joint.Joint(name=env_jnt_name)
            env_jnt.pos = result_jnt.pos
            env_jnt.rot = result_jnt.rot

            if result_jnt.parent:
                if cmds.objectType(result_jnt.parent) == 'joint':
                    env_jnt.parent = result_jnt.parent.replace('_jnt', '_env_jnt')
            else:
                env_jnt.parent = None
                cmds.parent(env_jnt, parent='world')

            cmds.makeIdentity(env_jnt, apply=True)

            cmds.connectAttr('{}.translate'.format(result_jnt), '{}.translate'.format(env_jnt))
            cmds.connectAttr('{}.rotate'.format(result_jnt), '{}.rotate'.format(env_jnt))
            cmds.connectAttr('{}.scale'.format(result_jnt), '{}.scale'.format(env_jnt))

            self.env_jnts.append(env_jnt)
        return self.env_jnts
