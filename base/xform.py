"""
Data object for representing a maya transform node
"""
import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
import maya.OpenMayaAnim as OpenMayaAnim
import xrig.util.maya_api as maya_api

"""
import sys
path = 'e:/'
if path not in sys.path:
    sys.path.append(path)   

import maya.cmds as cmds
import xrig.base.xform
reload(xrig.base.xform)
test = xrig.base.xform.Xform(name='test')
print test.translateX

test.tx = 1.125
cmds.select(test)

foo = xrig.base.xform.Xform(name='foo')
bar = xrig.base.xform.Xform(name='bar')


"""


class Xform(object):
    node_type = 'transform'
    rotate_order_dict = {0: 'xyz', 1: 'yzx', 2: 'zxy', 3: 'xzy', 4: 'yxz', 5: 'zyx'}
    display_type_dict = {0: 'normal', 1: 'template', 2: 'reference'}
    color_dict = {0: 'default', 1: 'black', 2: 'dark grey', 3: 'light grey', 4: 'burgundy', 5: 'dark blue', 6: 'blue',
                  7: 'dark green', 8: 'dark purple', 9: 'magenta', 10: 'beige', 11: 'dark beige', 12: 'brown',
                  13: 'red', 14: 'green', 15: 'sky blue', 16: 'white', 17: 'yellow', 18: 'cyan', 19: 'aqua', 20: 'pink',
                  21: 'skin', 22: 'pale yellow', 23: 'forest green', 24: 'copper', 25: 'gold', 26: 'sushi',
                  27: 'sea green', 28: 'keppel', 29: 'azure', 30: 'royal purple', 31: 'purple'}

    def __init__(self, name, *args, **kwargs):
        self.__dict__.update(kwargs)
        self.name = name

        if 'node_type' in kwargs:
            self.node_type = kwargs['node_type']
        if not self.node_type:
            self.node_type = 'transform'

        if not cmds.objExists(self.name):
            self.create_node()

        if cmds.objectType(self.name) and cmds.objectType(self.name) != self.node_type:
            self.create_node()

        for key, value in kwargs.items():
            try:
                if key == 'parent':
                    if kwargs.get('parent') != self.name:
                        self.parent = kwargs.get('parent')
                else:
                    if getattr(self, key):
                        setattr(self, key, value)
            except AttributeError:
                cmds.warning('Could not set value on attribute: {}.{}'.format(self, key))

        # --------------------------------------------------------------------------------------------------------------
        # --------------------------------------------------------------------------------------------------------------
        # --------------------------------------------------------------------------------------------------------------
        #
        #
        #
        # --------------------------------------------------------------------------------------------------------------
        # --------------------------------------------------------------------------------------------------------------
        # --------------------------------------------------------------------------------------------------------------

    #     for key, value in kwargs.items():
    #         try:
    #             if vars(self).get(key):
    #                 setattr(self, key, value)
    #             if getattr(self, key):
    #                 setattr(self, key, value)
    #         except AttributeError:
    #             cmds.warning('Could not set value on attribute: {}.{}'.format(self, key))
    #
    # def __getattr__(self, item):
    #     if cmds.attributeQuery(item, node=self.name, exists=True):
    #         return cmds.getAttr('{}.{}'.format(self.name, item))
    #     else:
    #         try:
    #             return self.__getitem__(item)
    #         except KeyError:
    #             raise AttributeError(item)
    #
    # def __setattr__(self, attr, value):
    #     try:
    #         cmds.setAttr('{}.{}'.format(self.name, attr), value)
    #     except Exception:
    #         object.__setattr__(self, attr, value)

    # --------------------------------------------------------------------------------------------------------------
    # --------------------------------------------------------------------------------------------------------------
    # --------------------------------------------------------------------------------------------------------------
    #
    #
    #
    # --------------------------------------------------------------------------------------------------------------
    # --------------------------------------------------------------------------------------------------------------
    # --------------------------------------------------------------------------------------------------------------

    def __repr__(self):
        return self.name

    def __str__(self):
        return str(self.name)

    def create_node(self):
        try:
            dag_mod = OpenMaya.MDagModifier()
            new_mobj = dag_mod.createNode(self.node_type)
            dag_mod.renameNode(new_mobj, self.name)
            dag_mod.doIt()
        except RuntimeError:
            dag_mod = OpenMaya.MFnDependencyNode()
            dag_mod.create(self.node_type, self.name)
        return True

    def rename_node(self, new_name):
        cmds.rename(self.name, new_name)

    # @classmethod
    # def clear_selection(cls):
    #     def wrapper(self, *args, **kwargs):
    #         cmds.select(clear=True)

    @property
    def shape(self):
        return maya_api.get_shapes(self.name)

    @property
    def parent(self):
        parent_node = cmds.listRelatives(self.name, parent=True)
        if parent_node:
            return parent_node[0]
        return None

    @parent.setter
    def parent(self, parent_node):
        try:
            cmds.parent(self.name, parent_node)
        except Exception as e:
            print('ERROR: {}'.format(e))

    # @property
    # def root(self):
    #     # parent_node = cmds.listRelatives(self.name, parent=True)
    #     # parent_node = self.parent
    #     if not self.parent:
    #         return self.name
    #     return self.root(self.parent)

    @property
    def child(self):
        return cmds.listRelatives(self.name, children=True)

    @property
    def children(self):
        return cmds.listRelatives(self.name, allDescendents=True)

    @property
    def color(self):
        if not cmds.getAttr('{}.overrideEnabled'.format(self.name.shape[0])):
            return 'default'
        return self.color_dict[cmds.getAttr('{}.overrideColor'.format(self.shape[0]))]

    @color.setter
    def color(self, value):
        for shape in self.shape:
            cmds.setAttr('{}.overrideEnabled'.format(shape), 1)
            cmds.setAttr('{}.overrideColor'.format(shape), value)

    @property
    def pos(self):
        return cmds.xform(self.name, translation=True, worldSpace=True, query=True)

    @pos.setter
    def pos(self, value):
        if len(value) == 2:
            translation, world_space = value
        elif len(value) == 3:
            translation, world_space = value, True
        elif len(value) == 4:
            translation, world_space = value[:3], value[-1]
        else:
            raise ValueError('Pass an iterable with one or two items')

        cmds.xform(self.name, translation=translation, worldSpace=world_space)

    @property
    def rot(self):
        return cmds.xform(self.name, rotation=True, worldSpace=True, query=True)

    @rot.setter
    def rot(self, value):
        if len(value) == 2:
            rotation, world_space = value
        elif len(value) == 3:
            rotation, world_space = value, True
        elif len(value) == 4:
            rotation, world_space = value[:3], value[-1]
        else:
            raise ValueError('Pass an iterable with one or two items')
        cmds.xform(self.name, rotation=rotation, worldSpace=world_space)

    @property
    def scale(self):
        return cmds.xform(self.name, scale=True, relative=True, query=True)

    @scale.setter
    def scale(self, value):
        if isinstance(value, int) or isinstance(value, float):
            value = (value, value, value)
        cmds.xform(self.name, scale=value)

    @property
    def rotate_order(self):
        rotate_order_int = cmds.getAttr('{}.rotateOrder'.format(self.name))
        return self.rotate_order_dict[rotate_order_int]

    @rotate_order.setter
    def rotate_order(self, rotate_order=None):
        if isinstance(rotate_order, str):
            for key, value in self.rotate_order_dict.items():
                if rotate_order == value:
                    cmds.setAttr('{}.rotateOrder'.format(self.name),
                                 self.rotate_order_dict.keys()[self.rotate_order_dict.values().index(rotate_order)])

        elif isinstance(rotate_order, int):
            cmds.setAttr('{}.rotateOrder'.format(self.name), rotate_order)

    def hide(self):
        return cmds.setAttr('{}.visibility'.format(self.name), False)

    def show(self):
        return cmds.setAttr('{}.visibility'.format(self.name), True)

    def freeze(self, translate=False, rotate=False, scale=False):
        cmds.makeIdentity(self.name, translate=translate, apply=True)
        cmds.makeIdentity(self.name, rotate=rotate, apply=True)
        cmds.makeIdentity(self.name, scale=scale, apply=True)
        return True

    def lock_attr(self, attr):
        cmds.setAttr('{}.{}'.format(self.name, attr), lock=True)

    def lock_and_hide_attr(self, attr):
        cmds.setAttr('{}.{}'.format(self.name, attr), lock=True, channelBox=False, keyable=False)

    def unhide_attr(self, attr):
        cmds.setAttr('{}.{}'.format(self.name, attr), channelBox=True, keyable=True)

    def unlock_and_unhide_attr(self, attr):
        cmds.setAttr('{}.{}'.format(self.name, attr), lock=False, channelBox=True, keyable=False)
